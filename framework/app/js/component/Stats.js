{
    SBUI.Stats = (function() {
        var instance;
        SBUI.Stats = function() {
            return instance;
        };
        SBUI.Stats.prototype = this;
        instance = new SBUI.Stats();
        instance.constructor = SBUI.Stats;
        return instance;
    });

    /**
     * Display total stats and current set stats
     */


    SBUI.Stats.prototype.displayStats = function(reset) {

        // if (reset) {
        //     for (i = 0; i < 4; i++) {
        //         jQuery('#stats-' + (i + 1) + ' .tries-' + (i + 1) + '-home').text(0);
        //         jQuery('#stats-' + (i + 1) + ' .tries-' + (i + 1) + '-away').text(0);
        //         SBUI.Stats.prototype.resizeBar(0, 0, 'tries-' + (i + 1) + '-bar');
        //
        //         jQuery('#stats-' + (i + 1) + ' .conversions-' + (i + 1) + '-home').text(0);
        //         jQuery('#stats-' + (i + 1) + ' .conversions-' + (i + 1) + '-away').text(0);
        //         SBUI.Stats.prototype.resizeBar(0, 0, 'conversions-' + (i + 1) + '-bar');
        //
        //         jQuery('#stats-' + (i + 1) + ' .scrums-won-' + (i + 1) + '-home').text(0);
        //         jQuery('#stats-' + (i + 1) + ' .scrums-won-' + (i + 1) + '-away').text(0);
        //         SBUI.Stats.prototype.resizeBar(0, 0, 'scrums-won-' + (i + 1) + '-bar');
        //
        //         jQuery('#stats-' + (i + 1) + ' .penalties-' + (i + 1) + '-home').text(0);
        //         jQuery('#stats-' + (i + 1) + ' .penalties-' + (i + 1) + '-away').text(0);
        //         SBUI.Stats.prototype.resizeBar(0, 0, 'penalties-' + (i + 1) + '-bar');
        //     }
        // }
        //

        // Total stats
        jQuery('#stats-total .home-total-goals').text(SBUI.Lineups.homeTotalGoals);
        jQuery('#stats-total .away-total-goals').text(SBUI.Lineups.awayTotalGoals);
        SBUI.Stats.prototype.resizeBar(SBUI.Lineups.homeTotalGoals, SBUI.Lineups.awayTotalGoals, 'goals-total-bar');

        jQuery('#stats-total .home-total-goals-on-pp').text(SBUI.Lineups.homeTotalGoalsOnPP);
        jQuery('#stats-total .away-total-goals-on-pp').text(SBUI.Lineups.awayTotalGoalsOnPP);
        SBUI.Stats.prototype.resizeBar(SBUI.Lineups.homeTotalGoalsOnPP, SBUI.Lineups.awayTotalGoalsOnPP, 'goals-on-pp-bar');

        jQuery('#stats-total .home-total-pp-minutes').text(SBUI.Lineups.homeTotalPPMinutes);
        jQuery('#stats-total .away-total-pp-minutes').text(SBUI.Lineups.awayTotalPPMinutes);
        SBUI.Stats.prototype.resizeBar(SBUI.Lineups.homeTotalPPMinutes, SBUI.Lineups.awayTotalPPMinutes, 'pp-minutes-bar');

        jQuery('#stats-total .home-total-penalties').text(SBUI.Lineups.homeTotalPenalties);
        jQuery('#stats-total .away-total-penalties').text(SBUI.Lineups.awayTotalPenalties);
        SBUI.Stats.prototype.resizeBar(SBUI.Lineups.homeTotalPenalties, SBUI.Lineups.awayTotalPenalties, 'penalties-total-bar');

        jQuery('#stats-total .home-total-on-target').text(SBUI.Lineups.homeTotalShotsOnTarget);
        jQuery('#stats-total .away-total-on-target').text(SBUI.Lineups.awayTotalShotsOnTarget);
        SBUI.Stats.prototype.resizeBar(SBUI.Lineups.homeTotalShotsOnTarget, SBUI.Lineups.awayTotalShotsOnTarget, 'on-target-total-bar');

        jQuery('#stats-total .home-total-off-target').text(SBUI.Lineups.homeTotalShotsOffTarget);
        jQuery('#stats-total .away-total-off-target').text(SBUI.Lineups.awayTotalShotsOffTarget);
        SBUI.Stats.prototype.resizeBar(SBUI.Lineups.homeTotalShotsOffTarget, SBUI.Lineups.awayTotalShotsOffTarget, 'off-target-total-bar');


        // Period stats
        for (i = 0; i < Math.min(SBUI.currentPeriodNumber, 4); i++) {

            jQuery('#stats-' + (i + 1) + ' .goals-' + (i + 1) + '-home').text(SBUI.Lineups.Stats[i + 1].homeGoals);
            jQuery('#stats-' + (i + 1) + ' .goals-' + (i + 1) + '-away').text(SBUI.Lineups.Stats[i + 1].awayGoals);
            SBUI.Stats.prototype.resizeBar(SBUI.Lineups.Stats[i + 1].homeGoals, SBUI.Lineups.Stats[i + 1].awayGoals, 'goals-' + [i + 1] + '-bar');

            jQuery('#stats-' + (i + 1) + ' .goals-' + (i + 1) + '-pp-home').text(SBUI.Lineups.Stats[i + 1].homeGoalsOnPP);
            jQuery('#stats-' + (i + 1) + ' .goals-' + (i + 1) + '-pp-away').text(SBUI.Lineups.Stats[i + 1].awayGoalsOnPP);
            SBUI.Stats.prototype.resizeBar(SBUI.Lineups.Stats[i + 1].homeGoalsOnPP, SBUI.Lineups.Stats[i + 1].awayGoalsOnPP, 'goals-' + [i + 1] + '-pp-bar');

            jQuery('#stats-' + (i + 1) + ' .pp-mins-' + (i + 1) + '-home').text(SBUI.Lineups.Stats[i + 1].homePPMinutes);
            jQuery('#stats-' + (i + 1) +  ' .pp-mins-' + (i + 1) + '-away').text(SBUI.Lineups.Stats[i + 1].awayPPMinutes);
            SBUI.Stats.prototype.resizeBar(SBUI.Lineups.Stats[i + 1].homePPMinutes, SBUI.Lineups.Stats[i + 1].awayPPMinutes, 'pp-mins-' + [i + 1] + '-bar');

            jQuery('#stats-' + (i + 1) + ' .penalties-' + (i + 1) + '-home').text(SBUI.Lineups.Stats[i + 1].homePenalties);
            jQuery('#stats-' + (i + 1) + ' .penalties-' + (i + 1) + '-away').text(SBUI.Lineups.Stats[i + 1].awayPenalties);
            SBUI.Stats.prototype.resizeBar(SBUI.Lineups.Stats[i + 1].homePenalties, SBUI.Lineups.Stats[i + 1].awayPenalties, 'penalties-' + (i + 1) + '-bar');

            jQuery('#stats-' + (i + 1) + ' .on-target-' + (i + 1) + '-home').text(SBUI.Lineups.Stats[i + 1].homeShotsOnTarget);
            jQuery('#stats-' + (i + 1) + ' .on-target-' + (i + 1) + '-away').text(SBUI.Lineups.Stats[i + 1].awayShotsOnTarget);
            SBUI.Stats.prototype.resizeBar(SBUI.Lineups.Stats[i + 1].homeShotsOnTarget, SBUI.Lineups.Stats[i + 1].awayShotsOnTarget, 'on-target-' + (i + 1) + '-bar');

            jQuery('#stats-' + (i + 1) + ' .off-target-' + (i + 1) + '-home').text(SBUI.Lineups.Stats[i + 1].homeShotsOffTarget);
            jQuery('#stats-' + (i + 1) + ' .off-target-' + (i + 1) + '-away').text(SBUI.Lineups.Stats[i + 1].awayShotsOffTarget);
            SBUI.Stats.prototype.resizeBar(SBUI.Lineups.Stats[i + 1].homeShotsOffTarget, SBUI.Lineups.Stats[i + 1].awayShotsOffTarget, 'off-target-' + (i + 1) + '-bar');

            // Active period highlight
            jQuery('.label-p' + (i + 1)).addClass('active');
            jQuery('.label-p' + (i)).removeClass('active');

            // Top period scores
            jQuery('.home-team-p' + (i + 1)).text(SBUI.Lineups.Stats[i + 1].homeGoals);
            jQuery('.away-team-p' + (i + 1)).text(SBUI.Lineups.Stats[i + 1].awayGoals);

            // Highest score highlight
            var homeScore = jQuery('.home-team-p'+ (i + 1));
            var awayScore = jQuery('.away-team-p' + (i + 1));

            jQuery('.home-team-p' + (i)).removeClass('active');
            jQuery('.away-team-p' + (i)).removeClass('active');

            if (jQuery(homeScore).text() > jQuery(awayScore).text()) {
                jQuery(homeScore).addClass('active');
            } else if (jQuery(awayScore).text() > jQuery(homeScore).text()) {
                jQuery(awayScore).addClass('active');
            } else {
                jQuery('.home-team-p' + (i)).removeClass('active');
                jQuery('.away-team-p' + (i)).removeClass('active');
            }

            if(SBUI.finished === true){
                jQuery('.label-p3').removeClass('active');
                jQuery('.label-p4').removeClass('active');
                jQuery('.home-team-p4').removeClass('active');
            }

        }
    };

    /**
     * Calculate stat bar width in percent
     */
    SBUI.Stats.prototype.resizeBar = function (left, right, barClass) {

        var leftNumber = parseFloat(left);
        var rightNumber = parseFloat(right);
        if ((leftNumber + rightNumber) !== 0) {
            var barLeft = (100 / (leftNumber + rightNumber)) * leftNumber;
            jQuery('.' + barClass).animate({width: barLeft + '%'}, 2500);
        } else if (leftNumber === 0 && rightNumber === 0) {
            jQuery('.' + barClass).animate({width: '50%'}, 2500);
        }
    };

}


