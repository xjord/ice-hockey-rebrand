{
    SBUI.Top = (function() {
        var instance;
        SBUI.Top = function() {
            return instance;
        };
        SBUI.Top.prototype = this;
        instance = new SBUI.Top();
        instance.constructor = SBUI.Top;
        return instance;
    });

    /**
     * Update player names
     * @param {Object} participants
     */
    SBUI.Top.prototype.updateTeamNames = function (teams) {
        //try {
            if(typeof teams[0] !== 'undefined'){
                if (typeof teams[0].team !== 'undefined') {
                    if (typeof teams[0].team.name !== 'undefined') {
                        SBUI.homeTeam = teams[0].team.name;
                    }
                    if (typeof teams[0].team._id !== 'undefined') {
                        SBUI.homeTeamID = teams[0].team._id;
                    }
                }
                /*if (typeof teams[0].gameData !== 'undefined' && typeof teams[0].gameData.powerPlay !== 'undefined') {
                    SBUI.homeTeamPowerplay = !teams[0].gameData.powerPlay;
                }*/
            }
            if(typeof teams[1] !== 'undefined'){
                if (typeof teams[1].team !== 'undefined') {
                    if (typeof teams[1].team.name !== 'undefined') {
                        SBUI.awayTeam = teams[1].team.name;
                    }
                    if (typeof teams[1].team._id !== 'undefined') {
                        SBUI.awayTeamID = teams[1].team._id;
                    }
                }
                /*if (typeof teams[1].gameData !== 'undefined' && typeof teams[1].gameData.powerPlay !== 'undefined') {
                    SBUI.awayTeamPowerplay = !teams[1].gameData.powerPlay;
                }*/
            }

            // Function to capitalise the first letter of each word
            function capitaliseFirst(string){
                var words = string.split(" ");
                var output = "";
                for (i = 0 ; i < words.length; i ++){
                    lowerWord = words[i].toLowerCase();
                    lowerWord = lowerWord.trim();
                    capitalisedWord = lowerWord.slice(0,1).toUpperCase() + lowerWord.slice(1);
                    output += capitalisedWord;
                    if (i != words.length-1){
                        output+=" ";
                    }
                }
                output[output.length-1] = '';
                return output;
            }

            //set names in top bar
            jQuery('.teamAname').text(capitaliseFirst(SBUI.homeTeam));
            jQuery('.teamBname').text(capitaliseFirst(SBUI.awayTeam));

            // //set names in bottom bar
            // jQuery('.teamAnamePoss').html(SBUI.homeTeam);
            // jQuery('.teamBnamePoss').html(SBUI.awayTeam);

            SBUI.__clearError(3010, 'Team names updated');
        /*} catch (err) {
            var msg = 'Could not update team names.';
            if (SBUI.Config.debug) {
                console.log(msg);
                console.log(err);
            }
            SBUI.__sendError(3010, msg);
        }*/
    };

    /**
     * Update match scores in top bar
     * @param {String} matchScores
     */
    SBUI.Top.prototype.updateMatchScore = function (matchScores) {
        //try {
            matchScores.split(':');
            const homeScore = matchScores[0];
            const awayScore = matchScores[2];
            jQuery('.home-team-total').text(homeScore);
            jQuery('.away-team-total').text(awayScore);
             SBUI.__clearError(3014, 'Match scores updated');
        /*} catch (err) {
            var msg = 'Could not update match scores';
            if (SBUI.Config.debug) {
                console.log(msg);
                console.log(err);
            }
            SBUI.__sendError(3014, msg);
        }*/
    };

    /**
     * Update footer bar message
     */
    SBUI.Top.prototype.updateTimerDisplays = function (timeLeft) {
        //try {
        if (SBUI.finished === true){
            jQuery('.latest-action').text("Match Finished");
            jQuery('.clock').text('Finished');
            jQuery('.label-ot').removeClass('active');
        }
        else{
            if (SBUI.currentPeriodNumber === 0) {
                jQuery('.clock').text('Pre-Game');
            }
            else {
                jQuery('.clock').text(SBUI.SocketService.prototype.replacePeriod() + ' ' + timeLeft);
            }
            jQuery('.latest-action').text(SBUI.SocketService.prototype.replacePeriodFull());
        }
        SBUI.__clearError(3016, 'Footer message updated');
        /*} catch (err) {
            var msg = 'Could not update footer message';
            if (SBUI.Config.debug) {
                console.log(msg);
                console.log(err);
            }
            SBUI.__sendError(3016, msg);
        }*/
    };
}