{
    SBUI.Animations = (function () {
        var instance;
        SBUI.Animations = function () {
            return instance;
        };
        SBUI.Animations.prototype = this;
        SBUI.Animations.secondaryBoard = false;
        /*SBUI.Animations.animating = {};
        SBUI.Animations.s = Snap("svg.animation");
        if (SBUI.Config.debug) {
            console.log('s type: ' + SBUI.Animations.s);
        }
        SBUI.Animations.s.attr({viewBox: "0 0 600 227"});*/
        instance = new SBUI.Animations();
        instance.constructor = SBUI.Animations;
        return instance;
    });

    jQuery.fn.rotate = function (degrees) {
        jQuery(this).css({
            '-webkit-transform': 'rotate(' + degrees + 'deg)',
            '-moz-transform': 'rotate(' + degrees + 'deg)',
            '-ms-transform': 'rotate(' + degrees + 'deg)',
            'transform': 'rotate(' + degrees + 'deg)'
        });
        return jQuery(this);
    };


    SBUI.Animations.prototype.removeAnimations = function () {

        // Remove all animations and messages
        jQuery('.action-message .badge').remove();
        jQuery('.action-message .words').remove();
        jQuery('.action-message .team').remove();
        SBUI.Animations.prototype.removeRectangular();
        jQuery('.action-message #svg1').remove();
        jQuery('.action-message #svg2').remove();
        jQuery('.action-text').removeClass('goal');
        jQuery('.possession').css('display', 'none');
        jQuery('.team').remove();
        if (jQuery('.action-message').text().length > 0) {
            jQuery('.action-text').text('');
        }

        SBUI.Animations.prototype.removeCircular();

    };

    SBUI.Animations.prototype.capitaliseFirst = function (string) {
        if (string != undefined) {
            var words = string.split(" ");
            var output = "";
            for (i = 0; i < words.length; i++) {
                lowerWord = words[i].toLowerCase();
                lowerWord = lowerWord.trim();
                capitalisedWord = lowerWord.slice(0, 1).toUpperCase() + lowerWord.slice(1);
                output += capitalisedWord;
                if (i != words.length - 1) {
                    output += " ";
                }
            }
            output[output.length - 1] = '';
            return output;
        }
    }

    SBUI.Animations.prototype.pitchMessage = function (badge, team, Message, homeAway) {

        SBUI.Animations.prototype.removeAnimations();
        jQuery('.action-text').text('');
        homeAway = SBUI.Animations.prototype.capitaliseFirst(homeAway);
        team = SBUI.Animations.prototype.capitaliseFirst(team);


        if(Message !== null){

            // If time stopped remove possession puck
            if (Message == "Time stopped" || "Timeout") {
                jQuery('.puck-home img').hide();
                jQuery('.puck-away img').hide();
            }

            // Add pitch message
            jQuery('.action-message').css('display', 'flex');
            jQuery('.action-message').append('<div class="badge"><span id="badgePic"><img src="images/Badges/' + badge + '"></span></div>');

            if (homeAway === 'Home') {
                jQuery('.team').css('display', 'flex');
                jQuery('.action-message').append('<div class="team home-won"><span class="team">' + team + '</span></br><span class="won-message">' + Message + '</span></div>');
            } else if (homeAway === 'Away') {
                jQuery('.team').css('display', 'flex');
                jQuery('.action-message').append('<div class="team away-won"><span class="team">' + team + '</span></br><span class="won-message">' + Message + '</span></div>');
            }

            if (Message !== 'Won Match') {
                jQuery('.action-message').append('<div class="words">' + Message + '</div>');
            }

            if (Message == 'Time stopped') {
                jQuery('.action-text').text('');
            }
        }
    };


    SBUI.Animations.prototype.bottomMessage = function (homeAway, team, Message) {

        // Remove previous messages and animations
        jQuery('.action-message .badge').remove();
        jQuery('.action-message .badge').remove();
        jQuery('.action-message .words').remove();
        jQuery('.action-message .team').remove();
        jQuery('.puck-home img').hide();
        jQuery('.puck-away img').hide();
        SBUI.Animations.prototype.removeCircular();
        jQuery('.team').remove();
        if (jQuery('.action-text').text().length > 0) {
            jQuery('.action-text').text('');
        }

        jQuery('.action-message').css('display', 'flex');

        team = SBUI.Animations.prototype.capitaliseFirst(team);
        homeAway = SBUI.Animations.prototype.capitaliseFirst(homeAway);

        if (homeAway === "Home") {
            jQuery('.action-text').append('<div class="action-home"><span class="top-message">' + team + ': </span><span class="bottom-message">' + Message + '</span></div>');
            jQuery('.action-home').css('display', 'inline-block');
            jQuery('.action-text').addClass('goal');

        } else if (homeAway === "Away") {
            jQuery('.action-text').append('<div class="action-away"><span class="top-message">' + team + ': </span><span class="bottom-message">' + Message + '</span></div>');
            jQuery('.action-away').css('display', 'inline-block');
            jQuery('.action-text').addClass('goal');
        }
    };

    SBUI.Animations.prototype.possessionPuck = function (homeAway) {

        if (homeAway === "home") {
            jQuery('.puck-home img').css('display','block');
            jQuery('.puck-away img').css('display','none');
        } else if (homeAway === "away") {
            jQuery('.puck-away img').css('display','block');
            jQuery('.puck-home img').css('display','none');
        }
    }

    //<editor-fold desc="goal animation">

    SBUI.Animations.prototype.possessionOverlay = function (homeAway) {

        SBUI.Animations.prototype.removeAnimations();

        jQuery('.possession').css('display','block');

        // Check window width
        SBUI.windowWidth = jQuery(window).width();

        // Possession animation on window resize
        jQuery(window).on('resize', function () {
            SBUI.windowWidth = jQuery(window).width();

            if (SBUI.windowWidth >= 668) {

                // Desktop
                if (homeAway === 'home') {
                    jQuery('.possession').addClass('possession-overlay-home');
                    jQuery('.possession').removeClass('possession-overlay-away');
                }
                else if (homeAway === 'away') {
                    jQuery('.possession').addClass('possession-overlay-away');
                    jQuery('.possession').removeClass('possession-overlay-home');
                }
            }
            else {
                // Mobile
                if (homeAway === 'home') {
                    jQuery('.possession').removeClass('possession-overlay-away');
                    jQuery('.possession').addClass('possession-overlay-home');
                } else if (homeAway === 'away') {
                    jQuery('.possession').addClass('possession-overlay-away');
                    jQuery('.possession').removeClass('possession-overlay-home');
                }
            }
        });


        // Possession animation on window load
        if (SBUI.windowWidth >= 668) {

            // Desktop
            if (homeAway === 'home') {
                jQuery('.possession').addClass('possession-overlay-home');
                jQuery('.possession').removeClass('possession-overlay-away');
            }
                else if (homeAway === 'away') {
                jQuery('.possession').addClass('possession-overlay-away');
                jQuery('.possession').removeClass('possession-overlay-home');
            }
        }
            else {
            // Mobile
            if (homeAway === 'home') {
                jQuery('.possession').removeClass('possession-overlay-away');
                jQuery('.possession').addClass('possession-overlay-home');
            } else if (homeAway === 'away') {
                jQuery('.possession').addClass('possession-overlay-away');
                jQuery('.possession').removeClass('possession-overlay-home');
            }
        }
    }

    SBUI.Animations.prototype.removeCircular = function () {
        if(typeof SBUI.cirlceElems !== 'undefined'){
            SBUI.cirlceElems.forEach(function(interval) {
                interval.stop();
                clearInterval(interval);
            });
        }
        if(SBUI.svgAnimCircular){
            SBUI.svgAnimCircular.remove();
            delete SBUI.svgAnimCircular;
        }
        jQuery('#svg1').remove();
    };

    /**
     * Display the circle pulse animation
     */
    SBUI.Animations.prototype.showCircular = function (homeAway, circleType) {

        SBUI.Animations.prototype.removeAnimations();

        SBUI.cirlceElems = [];

        var circleClass = circleType + homeAway;

        jQuery('.action-message').append("<svg id='svg1' class='svgActive'></svg>");
        jQuery('.action-message #svg1').addClass(circleClass);


        var svgSize = [50, 50]; //x & y size of the svg
        var animLength = 3000; //length in milliseconds
        var centreSize = svgSize[1] / 7; //ratio of the size of the centre circce
        var initStroke = 2; //set the inital stroke width
        var finalStroke = 2; //set the final stroke width

        var pos = [svgSize[1] / 2, svgSize[1] / 2]; //centre position

        SBUI.circularAnimation = Snap('#svg1').attr({ //creates the svg box
            width: svgSize[0],
            height: svgSize[1]
        });

        //create big circle in the middle:
        SBUI.centreCircle = SBUI.circularAnimation.circle(25, 25, centreSize).attr({
            fill: "white",
            fillOpacity: 1,
            stroke: "white",
            strokeWidth: initStroke
        });

        SBUI.r = SBUI.circularAnimation.circle(25, 25, 0).attr({
            fill: "red",
            fillOpacity: 0,
            stroke: "white",
            strokeWidth: initStroke,
            opacity: '1'
        });

        //initializes all the needed rings
        SBUI.ring = [];
        SBUI.ring[0] = SBUI.r;    //this is done this way otherwise the "r" circle was actually being displayed
        SBUI.ring[1] = SBUI.ring[0].clone();
        SBUI.ring[2] = SBUI.ring[0].clone();

        SBUI.svgAnimCircular = SBUI.circularAnimation.group(SBUI.ring[0], SBUI.ring[1], SBUI.ring[2], SBUI.centreCircle); //group the rings

        for (var i = 0; i < SBUI.ring.length; i++) { //this is basically adding an animation to delay
            SBUI.ring[i].animate({
                r: centreSize,
                strokeWidth: initStroke,
                opacity: '1'
            }, i * (animLength / SBUI.ring.length), function() {
                startRingAnim(this) //had to format the callback like this as the delay was not being applied
            })
        }

        function startRingAnim(ring)
        {
            SBUI.cirlceElems.push(ring)
            ;
            var arrPos = SBUI.cirlceElems.length-1;
            //console.log(SBUI.cirlceElems.length);
            anim(arrPos);

            function anim(arrPos) {
                try{
                    SBUI.cirlceElems[arrPos].animate({
                        r: 25,
                        strokeWidth: finalStroke,
                        opacity: '0'
                    }, animLength, mina.easeout, function () {
                        try {
                            this.animate({
                                r: centreSize,
                                strokeWidth: initStroke,
                                opacity: '1'
                            }, 0, function () {
                                /*if (SBUI.Config.debug) {
                                    console.log(arrPos);
                                }*/
                                try{
                                    anim(arrPos)
                                }catch(err){}
                            });
                        }catch(err){
                            if(SBUI.Config.debug){
                                console.log('circle anim err')
                            }
                        }
                    })
                }catch(err){}
            }
        }
    };

    /**
     * Display the triangle pulse animation
     * @param {interger} orient angle of the orientation starting from facing right
     */
    SBUI.Animations.prototype.removeRectangular = function () {
        if(typeof SBUI.rectangularElems !== 'undefined'){
            SBUI.rectangularElems.forEach(function(interval) {
                interval.stop();
                clearInterval(interval);
            });
        }
        if(SBUI.svgAnimAngular){
            SBUI.svgAnimAngular.remove();
            delete SBUI.svgAnimAngular;
        }
        jQuery('#svg2').remove();
    };


    SBUI.Animations.prototype.showRectangular = function (homeAway, rectType) {


        SBUI.Animations.prototype.removeAnimations();

        SBUI.rectangularElems = [];

        var classType = rectType + homeAway;

        jQuery('.action-message').append("<svg id='svg2' class='svgActive'></svg>");
        jQuery('#svg2').addClass(classType);

        // if('')


        var svgSize = [50, 50] //x & y size of the svg
        //var radious = svgSize[1]/2
        var animLength = 3000; //length in milliseconds

        var pos = [svgSize[1] / 2, svgSize[1] / 2] //centre position

        SBUI.rectangularAnimation = Snap('#svg2').attr({ //creates the svg box
            width: svgSize[0],
            height: svgSize[1]
        });

        //create big circle in the middle:
        SBUI.circlePin = SBUI.rectangularAnimation.circle(2.5, svgSize[1] / 2, 2.5).attr({
            fill: 'white',
            fillOpacity: 1,
            stroke: 'white',
            strokeWidth: 0
        });

        SBUI.initRect = SBUI.rectangularAnimation.rect(5, 0, 1, svgSize[1]).attr({
            fill: "white"
        });

        SBUI.r = SBUI.rectangularAnimation.rect(5, 0, 0, svgSize[1]).attr({
            fill: "white"
        });

        SBUI.mask = SBUI.rectangularAnimation.polygon([
            5, svgSize[1] / 2, //coordinates of each point of the mask
            svgSize[0], 0,
            svgSize[0], svgSize[1]
        ]).attr({
            fill: "yellow"
        });

        //initializes all the needed rings
        SBUI.rect = [];
        SBUI.rect[0] = SBUI.r;
        SBUI.rect[1] = SBUI.rect[0].clone();
        SBUI.rect[2] = SBUI.rect[0].clone();
        SBUI.rect[3] = SBUI.rect[0].clone();
        SBUI.rect[4] = SBUI.rect[0].clone();

        SBUI.pulseCone = SBUI.rectangularAnimation.group(SBUI.rect[0], SBUI.rect[1], SBUI.rect[2], SBUI.rect[3], SBUI.rect[4], SBUI.initRect,SBUI.mask); //group the lines
        SBUI.SvgAnimAngular = SBUI.rectangularAnimation.group(SBUI.pulseCone,SBUI.circlePin);

        SBUI.pulseCone.attr({
            mask: SBUI.mask
        })

        SBUI.initRect.animate({
            width: 15,
        }, 1400, mina.easeout, function() {
            for (var i = 0; i < SBUI.rect.length; i++) { //this is basically adding an animation to delay
                SBUI.rect[i].animate({
                    x: 16,
                    width: 5,
                    opacity: '1'
                }, i * (animLength / SBUI.rect.length), function() {
                    startRectAnim(this) //had to format the callback like this as the delay was not being applied
                })
            }
        });

        function startRectAnim(rect) {
            SBUI.rectangularElems.push(rect);
            var arrPos = SBUI.rectangularElems.length-1;
            anim(arrPos);
            function anim(arrPos) {
                //  if(typeof SBUI.rectangularElems !== 'undefined' || typeof SBUI.rectangularElems[arrPos] !== 'undefined') {
                try{
                    SBUI.rectangularElems[arrPos].animate({
                        x: svgSize[0],
                        width: 1,
                        opacity: '0'
                    }, animLength, mina.linear, function () {
                        try {
                            this.animate({
                                x: 14,
                                width: 5,
                                opacity: '1'
                            }, 0, function () {
                                try{
                                    anim(arrPos)
                                }catch(err){}
                            });
                        }catch(err){
                            if(SBUI.Config.debug){
                                console.log('rect anim err')
                            }
                        }
                    })
                }catch(err){}
            }
        }
    };



}