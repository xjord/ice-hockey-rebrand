/**
 * Config for framework.
 */

SBUI.Config.debug = true;
SBUI.Config.connection = 'socket';

//SBUI.Config.socketUrl = 'http://scoreboards.red7dev.com:8085';

//dev
SBUI.Config.socketUrl = 'https://scoreboards.red7dev.com';
SBUI.Config.streamingBaseUrl = 'https://streaming.williamhill-pp1.com';
SBUI.Config.streamingLibrary = 'https://sports-pp1.staticcache.org/streaming/streamPlayer.js';


//live
//SBUI.Config.socketUrl = 'https://scoreboards.red7mobile.com';
//SBUI.Config.streamingBaseUrl = 'https://streaming.williamhill.com';
//SBUI.Config.streamingLibrary = 'https://sports.staticcache.org/streaming/streamPlayer.js';