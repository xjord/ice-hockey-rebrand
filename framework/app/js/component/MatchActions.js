{
    SBUI.MatchActions = (function () {
        var instance;
        SBUI.MatchActions = function () {
            return instance;
        };
        SBUI.MatchActions.prototype = this;
        instance = new SBUI.MatchActions();
        instance.constructor = SBUI.MatchActions;
        return instance;
    });

    /**
     * Populate actions
     * @param {Object} actions
     */
    SBUI.MatchActions.prototype.populate = function (actions) {
//try {
        var keys = [];
        var count = 0;
        for (var key in actions) {
            count++;
            keys.push(key);
        }
        keys.sort();
        keys.pop();
        for (var i = 0; i < keys.length; i++) {
            this.add(actions[keys[i]], keys[i]);
        }
//add last action
        this.add(actions[key], key, true);

        SBUI.__clearError(3020, 'Match actions populated');
        /*} catch (err) {
        var msg = 'Could not populate match actions';
        if (SBUI.Config.debug) {
        console.log(msg);
        console.log(err);
        }
        SBUI.__sendError(3020, msg);
        }*/
    };

    /**
     * Display action with timed delay
     * singleton. Assuming you only need one copy of timed queue
     * @param {Object} actions
     */

    const timedQueue2 = (function () {
        var API;                // internal reference to interface
        const queue = [];       // array to hold functions
        var task = null;        // the next task to run
        var tHandle;            // To stop pending timeout
        function next() {  // runs current scheduled task and  creates timeout to schedule next
            if (task !== null) {          // is task scheduled??
                task.func();            // run it
                task = null;            // clear task
            }
            if (queue.length > 0) {       // are there any remain tasks??
                task = queue.shift();   // yes set as next task
                tHandle = setTimeout(next, task.time) // schedule when
            } else {
                API.done = true;
            }
        }

        return API = {
            add: function (func, time) {
                queue.push({func: func, time: time});
            },
            start: function () {
                if (queue.length > 0 && API.done) {
                    API.done = false;   // set state flag
                    tHandle = setTimeout(next, 0);
                }
            },
            clear: function () {
                task = null;            // remove pending task
                queue.length = 0;       // empty queue
                clearTimeout(tHandle);  // clear timeout
                API.done = true;        // set state flag
            },
            done: true
        }
    })();

    /**
     * Pas actions to display function
     * @param {Object} action
     * @param {String} key
     * @param {Boolean} animate
     */
    SBUI.MatchActions.prototype.add = function (action, key, animate) {
        if (animate) {
            function showActionTimed() {
                SBUI.MatchActions.prototype.sendToDisplay(action, animate);
            }

            timedQueue2.add(showActionTimed, 2000);
            timedQueue2.start();
        } else {
            SBUI.MatchActions.prototype.sendToDisplay(action, animate);
        }
    };

    /**
     * Display action and blackboards
     * @param {Object} action
     * @param {Boolean} animate
     */
    SBUI.MatchActions.prototype.sendToDisplay = function (action, animate) {


        var actionType = '';

        //show blackboards and badges
        if (typeof action.teamId !== 'undefined')
        {
        //get team name
            var team = (action.teamId === SBUI.homeTeamID ? SBUI.homeTeam : SBUI.awayTeam);
        //is this a home or away action?
            var homeAway = (action.teamId === SBUI.homeTeamID ? 'home' : 'away');
        }

        // Winning team message
        if(SBUI.finished === true){

            var homeScore = jQuery('.home-team-total').text();
            var awayScore = jQuery('.away-team-total').text();
            var homeTeam = jQuery('.teamAname').text();
            var awayTeam = jQuery('.teamBname').text();


            if(homeScore > awayScore){
                SBUI.Animations.prototype.pitchMessage('win.png', homeTeam, "Won Match", "Home");
            } else if(awayScore > homeScore){
                SBUI.Animations.prototype.pitchMessage('win.png', awayTeam, "Won Match", "Away");
            }
        }

        switch (action.actionId) {

            case '30':
                //goal!
                actionType = 'goal';
                if (animate) {
                    SBUI.Animations.prototype.showRectangular(homeAway, 'goal-');
                    SBUI.Animations.prototype.bottomMessage(homeAway, team, 'Goal Scored');
                }
                break;
            case '60':
                //substitution
                actionType = 'time';
                if (animate) {
                    SBUI.Animations.prototype.showBlackBoard('SUBSTITUTION', team, 'substitution', '4000');
                    //TODO
                    //show badge???
                }
                break;
            case '110':
                //change possession
                actionType = 'possession' + homeAway;

                // Record most recent possession
                SBUI.recentPossession = homeAway;
                SBUI.recentPossessionTeam = team;

                if(animate){
                    SBUI.Animations.prototype.bottomMessage(homeAway, team, 'In Possession');
                    SBUI.Animations.prototype.possessionOverlay(homeAway);
                    SBUI.Animations.prototype.possessionPuck(homeAway);
                }
                break;
            case '155':
                //shot on target
                actionType = 'shot';
            case '156':
                //shot off target
                actionType = 'shot';
                break;
            case '161':
                //penalty awarded
                actionType = 'shot';
                if (animate) {
                    SBUI.Animations.prototype.bottomMessage(homeAway, team, 'Penalty Shot');
                    SBUI.Animations.prototype.showCircular(homeAway, 'penalty-');
                }
                break;
            case '666':
                //penalty missed
                actionType = 'miss';
                break;
            case '1002':
                //penalty shot
                actionType = 'shot';
                break;
            case '1013':
                actionType = 'time';
                switch (action.info) {
                    case 'FIRST_PERIOD':
                        this.newPeriodStarted(action, animate);

                        break;
                    case 'SECOND_PERIOD':
                        this.newPeriodStarted(action, animate);

                        break;
                    case 'THIRD_PERIOD':
                        this.newPeriodStarted(action, animate);

                        break;
                    case 'FIRST_PAUSE':
                            SBUI.Animations.prototype.pitchMessage('time_stopped.png', null, 'End of period 1', null);
                        break;
                    case 'SECOND_PAUSE':
                            SBUI.Animations.prototype.pitchMessage('time_stopped.png', null, 'End of period 2', null);
                        break;
                    case 'ENDED':
                        if (animate) {
                            if (typeof SBUI.possessionOverlay !== 'undefined') {
                                SBUI.possessionOverlay.animate({opacity: 0}, 500, function () {
                                    SBUI.possessionOverlay.hide();
                                });
                            }
                        }
                        break;
                    case 'OVERTIME':
                        SBUI.Animations.prototype.pitchMessage('overtime.png', team, 'Overtime', null);
                        this.newPeriodStarted(action, animate);
                        if (animate) {
                            if (typeof SBUI.possessionOverlay !== 'undefined') {
                                SBUI.possessionOverlay.show();
                            }
                        }
                        break;
                    case 'AFTER_OT':
                        if (animate) {
                            if (typeof SBUI.possessionOverlay !== 'undefined') {
                                SBUI.possessionOverlay.animate({opacity: 0}, 500, function () {
                                    SBUI.possessionOverlay.hide();
                                });
                            }
                        }
                        break;
                    case 'AWAITING_OT':
                        if (animate) {
                            if (typeof SBUI.possessionOverlay !== 'undefined') {
                                SBUI.possessionOverlay.animate({opacity: 0}, 500, function () {
                                    SBUI.possessionOverlay.hide();
                                });
                            }
                        }
                        break;
                    case 'AWAITING_PENALTIES':
                        if (animate) {
                            if (typeof SBUI.possessionOverlay !== 'undefined') {
                                SBUI.possessionOverlay.animate({opacity: 0}, 500, function () {
                                    SBUI.possessionOverlay.hide();
                                });
                            }
                        }
                        break;
                    case 'PENALTY_SHOOTING':
                        this.newPeriodStarted(action, animate);
                        if (animate) {

                        }
                        break;
                    case 'AFTER_PENALTIES':
                        if (animate) {

                        }
                        break;
                }
                break;
            case '1015':
                //players on rink
                actionType = 'time';
                break;
            case '1018':
                //possible goal
                actionType = 'shot';
                SBUI.Animations.prototype.showRectangular(homeAway, 'goal-');
                SBUI.Animations.prototype.bottomMessage(homeAway, team, 'Possible Goal');
                break;
            case '1019':
                actionType = 'miss';
                break;
            case '1024':
                //match about to start
                actionType = 'time';
                break;
            case '1035':
                //timeout
                actionType = 'timeout';
                SBUI.Animations.prototype.pitchMessage('timeout.png', team, 'Timeout', null);
                break;
            case '1036':
                //paused/unpaused
                var pausing = action.description.endsWith('stopped');
                actionType = 'time' + (pausing ? 'out' : '');
                this.pauseUnpause(action, animate);

                // check if game stopped or running
                if(pausing === true){
                    SBUI.Animations.prototype.pitchMessage('time_stopped.png', team, 'Time stopped', null);
                } else if(pausing === false){

                    // Time running again
                    //Get recent possession variables
                    var homeAway = SBUI.recentPossession;
                    var team = SBUI.recentPossessionTeam;

                    //Display latest possession when time running again so no blank screens
                    SBUI.Animations.prototype.bottomMessage(homeAway, team, 'In Possession');
                    SBUI.Animations.prototype.possessionOverlay(homeAway);
                    SBUI.Animations.prototype.possessionPuck(homeAway);
                }
                break;
            case '1039':
                actionType = 'time';
                break;
            case '1042':
                //penalty awarded
                actionType = 'shot';
                break;
            case '1047':
                //timeout over
                actionType = 'time';
                break;
            case '1049':
                //suspension over
                actionType = 'time';
                if (animate) {
                    SBUI.Animations.prototype.showRectangular(homeAway, 'suspension-over-');
                    SBUI.Animations.prototype.bottomMessage(homeAway, team, 'Suspension Over');
                }
                break;
            case '1050':
                //possible empty net
                actionType = 'emptynet';
                break;
            case '1051':
                //empty net
                actionType = 'emptynet';
                if (animate) {
                    SBUI.Animations.prototype.bottomMessage(homeAway, team, 'Empty Net');
                    SBUI.Animations.prototype.showCircular(homeAway, 'empty-net-');
                }
                break;
            case '1052':
                //empty net over
                actionType = 'emptynet';
                break;
            case '1104':
                //penalty shootout starting
                actionType = 'shot';
                break;
            case '1112':
                actionType = 'time';
                break;
            case '1113':
                actionType = 'time';
                break;
            case '1437':
                //tv timeout
                actionType = 'timeout';
                SBUI.Animations.prototype.pitchMessage('timeout.png', team, 'Timeout', null);
                break;
            case '1438':
                //tv timeout over
                actionType = 'time';
                SBUI.Animations.prototype.pitchMessage('timeout.png', team, 'Timeout over', null);
                break;
            case '1521':
                actionType = 'timeout';
                break;
            case '1768':
                actionType = 'shot';
                break;
            case '1769':
                actionType = 'time';
                break;
            case '1770':
                //stoppage
                actionType = 'timeout';
                break;
            case '1771':
                //faceoff
                actionType = 'shot';
                break;
            case '1772':
                actionType = 'shot';
                break;
            case '1885':
                //puck position
                break;
            default:
                actionType = 'time';
                break;
        }
        //</editor-fold>

        if (animate) {

            // If period pre-game then set time as 20:00
            if(action.period === "0"){
                jQuery('#comment_container .no_comments').remove();
                jQuery('<li style="padding-left: 0"><table><tbody><tr><td><span>- P1</span></td><td><span class="time" style="padding-left: 9px">20:00</span></td><td style="padding-left: 8px">' + this.getCommentaryText(action) + '</td></tr></tbody></table></li>').prependTo('#comment_container ul').hide().fadeIn(500);
            }
            else {
                jQuery('#comment_container .no_comments').remove();
                jQuery('<li><table><tbody><tr><td><span class="period">- ' + SBUI.MatchActions.prototype.replacePeriodCommentary(action.period) + '</span></td><td><span class="time">' + SBUI.MatchActions.prototype.getActionMins(action) + SBUI.MatchActions.prototype.getActionSecs(action) + '</span></td><td>' + this.getCommentaryText(action) + '</td></tr></tbody></table></li>').prependTo('#comment_container ul').hide().fadeIn(500);
            }
        }
        else {
            if(action.period === "0"){
                jQuery('<li style="padding-left: 0"><table><tbody><tr><td><span>- P1</span></td><td><span class="time" style="padding-left: 9px">20:00</span></td><td style="padding-left: 8px">' + this.getCommentaryText(action) + '</td></tr></tbody></table></li>').prependTo('#comment_container ul').hide().fadeIn(500);
            }
            else {
                jQuery('<li><table><tbody><tr><td><span class="period">- ' + SBUI.MatchActions.prototype.replacePeriodCommentary(action.period) + '</span></td><td><span class="time">' + SBUI.MatchActions.prototype.getActionMins(action) + SBUI.MatchActions.prototype.getActionSecs(action) + '</span></td><td>' + this.getCommentaryText(action) + '</td></tr></tbody></table></li>').prependTo('#comment_container ul');
            }

        }

        // To add icons:
        // '\</span></span><img class="icon" src="' + this.getIcon(actionType) + '"></img></td><td>' +

        //</editor-fold>

        SBUI.__clearError(3021, 'Action display updated');
        /*} catch (err) {
        var msg = 'Could not update action display message';
        if (SBUI.Config.debug) {
        console.log(msg);
        console.log(err);
        }
        SBUI.__sendError(3021, msg);
        }*/
    };

//<editor-fold desc="Stat Actions">

    //whenever the other team takes the puck
    SBUI.MatchActions.prototype.changePossession = function (action, animate) {
        if (typeof action !== 'undefined' && typeof action.teamId !== 'undefined') {
            var showPuck = (action.teamId === SBUI.homeTeamID ? jQuery('.home-puck-image') : jQuery('.away-puck-image'));
            var hidePuck = (action.teamId === SBUI.homeTeamID ? jQuery('.away-puck-image') : jQuery('.home-puck-image'));
            SBUI.possessionOverlay = (action.teamId === SBUI.homeTeamID ? jQuery('.home-possession-overlay') : jQuery('.away-possession-overlay'));
            var hideOverlay = (action.teamId === SBUI.homeTeamID ? jQuery('.away-possession-overlay') : jQuery('.home-possession-overlay'));
            SBUI.animateOverlay = false;
            //showOverlay.show();
            //hideOverlay.hide();
            //if it's the latest action, do a swanky lil animation
            if (animate) {
                hidePuck.animate({height: '+=5px', width: '+=5px'}, 500, function () {
                    hidePuck.animate({height: '0px', width: '0px'}, 500, function () {
                        hidePuck.hide();
                        hidePuck.css({height: '15px', width: '20px'});
                        showPuck.css({height: '0px', width: '0px'});
                        showPuck.show();
                        showPuck.animate({height: '20px', width: '25px'}, 500, function () {
                            showPuck.animate({height: '-=5px', width: '-=5px'}, 500, function () {
                                showPuck.animate({height: '+=5px', width: '+=5px'}, 500, function () {
                                    showPuck.animate({height: '-=5px', width: '-=5px'}, 500);
                                });
                            });
                        })
                    });
                })
                hideOverlay.animate({opacity: 0}, 500, function () {
                    hideOverlay.hide();
                    SBUI.possessionOverlay.css({opacity: 0});
                    SBUI.possessionOverlay.show();
                    SBUI.possessionOverlay.animate({opacity: 1}, 500, function () {
                        SBUI.possessionOverlay.animate({opacity: 0.6}, 500, function () {
                            SBUI.possessionOverlay.animate({opacity: 1}, 500, function () {
                                SBUI.possessionOverlay.animate({opacity: 0.6}, 500, function () {
                                    SBUI.possessionOverlay.animate({opacity: 1}, 500, function () {
                                        SBUI.possessionOverlay.animate({opacity: 0.6}, 500, function () {
                                            SBUI.possessionOverlay.animate({opacity: 1}, 500, function () {
                                                SBUI.animateOverlay = true;
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    })
                })
            }
            //otherwise just hide/show without fading
            else {
                SBUI.possessionOverlay.show();
                SBUI.possessionOverlay.css({opacity: 1});
                hideOverlay.hide();
                showPuck.show();
                hidePuck.hide();
                SBUI.animateOverlay = true;
            }
        }
        else {
            //hide both on reset
            jQuery('.away-puck-image').hide();
            jQuery('.home-puck-image').hide();
            jQuery('.away-possession-overlay').hide();
            jQuery('.home-possession-overlay').hide();
            SBUI.animateOverlay = false;
        }
    }

    //when a goal is scored...
    SBUI.MatchActions.prototype.goalScored = function (action) {
        if (typeof action.teamId !== 'undefined' && typeof action.period !== 'undefined') {
            if (action.teamId === SBUI.homeTeamID) {
                SBUI.Lineups.homeTotalGoals++;
                SBUI.Lineups.Stats[SBUI.SocketService.prototype.getPeriodNumber(action.period)].homeGoals ++;
                if (typeof action.goalType !== 'undefined' && action.goalType === 'Power play')
                {
                    SBUI.Lineups.homeTotalGoalsOnPP ++;
                    SBUI.Lineups.Stats[SBUI.SocketService.prototype.getPeriodNumber(action.period)].homeGoalsOnPP ++;
                }
            }
            else {
                SBUI.Lineups.awayTotalGoals++;
                SBUI.Lineups.Stats[SBUI.SocketService.prototype.getPeriodNumber(action.period)].awayGoals ++;
                if (typeof action.goalType !== 'undefined' && action.goalType === 'Power play')
                {
                    SBUI.Lineups.awayTotalGoalsOnPP ++;
                    SBUI.Lineups.Stats[SBUI.SocketService.prototype.getPeriodNumber(action.period)].awayGoalsOnPP ++;
                }
            }
            jQuery('.' + this.replacePeriodScores(action.period) + 'Score').text(SBUI.Lineups.Stats[SBUI.SocketService.prototype.getPeriodNumber(action.period)].homeGoals + ' : ' + SBUI.Lineups.Stats[SBUI.SocketService.prototype.getPeriodNumber(action.period)].awayGoals);
        }
    }

    SBUI.MatchActions.prototype.shotOnTarget = function (action) {
        if (typeof action.teamId !== 'undefined' && action.period !== 'undefined') {
            if (action.teamId === SBUI.homeTeamID) {
                SBUI.Lineups.homeTotalShotsOnTarget ++;
                SBUI.Lineups.Stats[SBUI.SocketService.prototype.getPeriodNumber(action.period)].homeShotsOnTarget ++;
            }
            else {
                SBUI.Lineups.awayTotalShotsOnTarget ++;
                SBUI.Lineups.Stats[SBUI.SocketService.prototype.getPeriodNumber(action.period)].awayShotsOnTarget ++;
            }
        }
    }

    SBUI.MatchActions.prototype.shotOffTarget = function (action) {
        if (typeof action.teamId !== 'undefined' && action.period !== 'undefined') {
            if (action.teamId === SBUI.homeTeamID) {
                SBUI.Lineups.homeTotalShotsOffTarget ++;
                SBUI.Lineups.Stats[SBUI.SocketService.prototype.getPeriodNumber(action.period)].homeShotsOffTarget ++;
            }
            else {
                SBUI.Lineups.awayTotalShotsOffTarget ++;
                SBUI.Lineups.Stats[SBUI.SocketService.prototype.getPeriodNumber(action.period)].awayShotsOffTarget ++;
            }
        }
    }

    SBUI.MatchActions.prototype.newPeriodStarted = function (action, animate) {

        if (typeof action.period !== 'undefined')

        {
            var tab;
            if (!jQuery('.' + this.replacePeriodScores(action.period) + 'Score').length || jQuery('.' + this.replacePeriodScores(action.period) + 'Score').text() === '-') {
                switch (action.period) {
                    case '1' :
                        if(!(jQuery('#stats-1-tab').length > 0)) {
                            tab = jQuery('.tab-list').append("<li><a id='stats-1-tab' href='#' data-toggle='stats-1'><span>Period 1</span></li>");
                        }
                        break;
                    case '2' :
                        if(!(jQuery('#stats-2-tab').length > 0)) {
                            tab = jQuery('.tab-list').append("<li><a id='stats-2-tab' href='#' data-toggle='stats-2'><span>Period 2</span></a></li>");
                        }
                        break;
                    case '3' :
                        if(!(jQuery('#stats-3-tab').length > 0)){
                            tab = jQuery('.tab-list').append("<li><a id='stats-3-tab' href='#' data-toggle='stats-3'><span>Period 3</span></a></li>");
                        }
                        break;
                    // case 'ST':
                    //     tab = jQuery('.tab-list').append("<li><a href='#' data-toggle='stats-4'><span>SO</span></a></li>");
                    //     jQuery('.scoreHeaders').append("<th class=\"SOHeader\"><div>Shoot Out</div></th>");
                    //     jQuery('.individualScores').append("<td class=\"SOScore\">-</td>");
                    //     break;
                    default:

                        if(!(jQuery('#OT').length > 0)) {
                            if (action.period.startsWith("OT")) {
                                tab = jQuery('.tab-list').append("<li><a id='OT' href='#' data-toggle='stats-4'><span>OT</span></a></li>");
                                jQuery('.score-headers').append('<div class="label label-p4">O1</div>');
                                jQuery('.home-team-points').append('<div class="home-team-p4">0</div>');
                                jQuery('.away-team-points').append('<div class="away-team-p4">0</div>');
                            }
                        }
                        break;
                }
            }
            jQuery('.' + this.replacePeriodScores(action.period) + 'Score').text('0 : 0');
            jQuery('.' + this.replacePeriodScores(action.period) + 'Score').css({'color' : 'white'});
            jQuery('.' + this.replacePeriodScores(action.period) + 'Header').css({'color' : 'black'});
            if (animate) {
                /*if (typeof tab !== 'undefined') {
                var newWidth = jQuery(currentTabs[0]).css("width");
                currentTabs.css({'width': prevWidth});
                tab.css({'opacity': 0, 'width': 0});
                currentTabs.animate({'width': newWidth}, 1000, function () {
                tab.animate({opacity: 1, width: newWidth}, 1000);
                })
                }*/
                if (typeof tab !== 'undefined') {
                    tab.css({'opacity': 0});
                    tab.animate({opacity: 1}, 1000);
                }
                SBUI.Top.prototype.updateTimerDisplays('20:00');
            }
        }
    }

    SBUI.MatchActions.prototype.penaltyTaken = function (action) {
        if (typeof action.teamId !== 'undefined' && typeof action.period !== 'undefined')
        {
            if (action.teamId === SBUI.homeTeamID) {
                SBUI.Lineups.homeTotalPenalties++;
                SBUI.Lineups.Stats[SBUI.SocketService.prototype.getPeriodNumber(action.period)].homePenalties ++;
            }
            else {
                SBUI.Lineups.awayTotalPenalties++;
                SBUI.Lineups.Stats[SBUI.SocketService.prototype.getPeriodNumber(action.period)].awayPenalties ++;
            }
        }
    }

    SBUI.MatchActions.prototype.suspensionGiven = function (action) {
        if (typeof action.teamId !== 'undefined' && typeof action.period !== 'undefined' && typeof action.minutesSuspended !== 'undefined')
        {
            /*if (action.teamId === SBUI.homeTeamID) {
            SBUI.Lineups.homeTotalPPMinutes += parseInt(action.minutesSuspended);
            SBUI.Lineups.Stats[SBUI.SocketService.prototype.getPeriodNumber(action.period)].homePPMinutes += parseInt(action.minutesSuspended);
            }
            else {
            SBUI.Lineups.awayTotalPPMinutes += parseInt(action.minutesSuspended);
            SBUI.Lineups.Stats[SBUI.SocketService.prototype.getPeriodNumber(action.period)].awayPPMinutes += parseInt(action.minutesSuspended);
            }*/
            if (action.teamId === SBUI.awayTeamID) {
                SBUI.Lineups.homeTotalPPMinutes += parseInt(action.minutesSuspended);
                SBUI.Lineups.Stats[SBUI.SocketService.prototype.getPeriodNumber(action.period)].homePPMinutes += parseInt(action.minutesSuspended);
            }
            else {
                SBUI.Lineups.awayTotalPPMinutes += parseInt(action.minutesSuspended);
                SBUI.Lineups.Stats[SBUI.SocketService.prototype.getPeriodNumber(action.period)].awayPPMinutes += parseInt(action.minutesSuspended);
            }
        }
        // SBUI.Stats.prototype.displayStats();
    }

    SBUI.MatchActions.prototype.pauseUnpause = function (action, animate) {
        if (typeof action.description !== 'undefined')
        {
            SBUI.Lineups.paused = (action.description.endsWith('stopped'));
            if (animate) {
                if (SBUI.Lineups.paused == true) {
                    SBUI.possessionOverlay.animate({opacity: 0}, 500, function () {
                        SBUI.possessionOverlay.hide();
                    });
                }
                else {
                    SBUI.possessionOverlay.show();
                }
            }
        }
    }

    //</editor-fold>

//<editor-fold desc="String Replacement & Commentary Functions">

    //for replacing the period notations for the commentary box
    SBUI.MatchActions.prototype.replacePeriodCommentary = function (period) {
        switch (period) {
            case "0":
                return 'Pre-game'
            case "1":
                return 'P1';
            case "2":
                return 'P2';
            case "3":
                return 'P3';
            case 'shootout':
                return 'SO';
            default:
                if (period.startsWith("OT"))
                    return 'O' + period.substring(2);
                else
                    return '???';
        }
    }

    //For replacing the period notations for the small top scoreboard
    SBUI.MatchActions.prototype.replacePeriodScores = function (period) {

        switch (period){
            case '0':
                return "0";
            case '1':
                return "1st";
            case '2':
                return "2nd";
            case '3':
                return "3rd";
            case 'ST':
                return "SO";
            default:
                if (period.startsWith("OT")) {
                    return "OT";
                }
                else
                {
                    return "0";
                }
        }
    }

    //for breaking down the timers for commentary purposes
    SBUI.MatchActions.prototype.getActionMins = function (action) {
        //we don't need a timer for pre-game actions
        if (action.period === "0")
            return ''
        else {

            var time = action.time;
            var mins = 19 - (time.split(":")[0] % 20);
            if (time.split(":")[1] === '00') {
                mins ++;
            }
            if (mins.toString().length === 1) {
                mins = '0' + mins;
            }
            return mins + ":";
        }
    }

    //for getting the accompanying commentary text
    SBUI.MatchActions.prototype.getCommentaryText = function (data) {

        if (typeof data.description !== 'undefined') {

            //Find and decapitalise uppercase words
            var words = data.description.split(" ");

                for(i = 0; i < words.length; i ++){

                    if(words[i].match(/\b[A-Z]+\b/g)){
                        lowerWord = words[i].toLowerCase();
                        lowerWord = lowerWord.trim();
                        capitalisedWFirstLetter = lowerWord.slice(0, 1).toUpperCase() + lowerWord.slice(1);

                        var capitalisedWord = words.indexOf(words[i])

                        if(capitalisedWord !== -1){
                            words[capitalisedWord] = capitalisedWFirstLetter
                        }

                        var string = words.join(" ")


                        if(!(string.match(/\b[A-Z]+\b/g))){
                            return string;

                        }
                    } else {
                        return data.description;
                    }
                }

            }
        }

    //as above...
    SBUI.MatchActions.prototype.getActionSecs = function (action) {
        if (action.period === "0")
            return ''
        else {
            var secs = action.time;
            secs = 60 - secs.split(":")[1];
            if (secs === 60) {
                secs = 0;
            }
            if (secs.toString().length === 1) {
                secs = '0' + secs;
            }
            return secs;
        }
    }

    //</editor-fold>
}