/**
 * Initialisation for framework.
 */

/**
 * Setup main SBUI namespace
 */
window.SBUI = this["SBUI"] || {}; // Closure will remove empty objects during optimisation
this["SBUI"] = window.SBUI;
window.SBUI = window.SBUI || {};

// SBUI.Initialise = false;

// Set screen breakpoint
SBUI.breakPoint = 668;

/**
 * Setup Config Namespace
 */
SBUI.Config = function() {};

/**
 * Setup Errors Namespace
 */
SBUI.Errors = function() {};

//initial scoreboard height
SBUI.__height = 0;
SBUI.showingUI = false;

/**
 * Read the parameters passed in the URL.
 *
 * Usage:
 *     utils.getUrlParameters().eventId
 *    or
 *     var params = utils.getUrlParameters(),
 *         eventId = objParams.eventId,
 *         locale = objParams.locale;
 *
 * @return {Object} an object will all parameters
 */

SBUI.__getUrlParameters = function () {

    var match;
    var pl = /\+/g; // Regex for replacing addition symbol with a space
    var search = /([^&=]+)=?([^&]*)/g;
    var decode = function (s) {
        return decodeURIComponent(s.replace(pl, ' '));
    };

    var urlParams = {};

    while (match = search.exec(window.location.search.substring(1))) {    // jshint ignore: line
        urlParams[decode(match[1])] = decode(match[2]);
    }

    return urlParams;
};

/**
 * Get URL parameters for the Config
 */
SBUI.Config.params = SBUI.__getUrlParameters();

/**
 * Configure debugging
 */
if (typeof SBUI.Config.debug === 'undefined' || SBUI.Config.debug !== true) {
    SBUI.Config.debug = false;
}

/**
 * Get the number of properties in an object.
 *
 * @param obj
 * @returns {Number}
 */
SBUI.__countProperties = function(obj) {

    if (obj !== null && typeof obj === 'object') {
        return Object.keys(obj).length;
    } else {
        return 0;
    }
};

/**
 * Change the first character of a string to uppercase.
 *
 * @param string
 * @returns {string}
 * @private
 */
SBUI.__ucfirst = function(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
};

/**
 * Get the index of an object.
 *
 * @param obj
 * @param search
 * @returns {*}
 * @private
 */
SBUI.__indexOf = function(obj, search) {

    if (typeof obj !== 'object') {
        return false;
    }

    for (var i in obj) {
        if (obj[i] == search) {
            return i;
        }
    }

    return false;
};

/**
 * Create a GUID
 *
 * @returns {string}
 * @private
 */
SBUI.__guid = function () {

    var d = new Date().getTime();

    if(window.performance && typeof window.performance.now === "function"){
        d += performance.now(); //use high-precision timer if available
    }

    var uuid = 'xxxx-xxxx-4xxx-yxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
};

/**
 * Post a json message to the iFrame parent window
 *
 * @param msg
 * @private
 */
SBUI.__postToParent = function (msg) {

    console.log({referer: this.Config.params.referer, msg: msg});
    window.parent.postMessage(msg, this.Config.params.referer);
};

/**
 * Send a resize message to the iFrame parent if the size has changed
 *
 * @private
 */
SBUI.__sendResize = function () {

    var height = $('#main-wrapper').height()+3; //TODO: temp fix, might be worth more investigation
    if (typeof SBUI.__height != 'undefined' && SBUI.__height != height) {
        var msg = {
            id: this.__guid(),
            type: 'setHeight',
            eventId: SBUI.Config.eventId,
            data: {
                height: height
            }
        };

        this.__postToParent(msg);

        var msg = {
            id: this.__guid(),
            method: 'setHeight',
            eventId: SBUI.Config.eventId,
            height: height
        };

        this.__postToParent(msg);

        SBUI.__height = height;
    }
};

/**
 * Send error to parent
 *
 * @private
 */
SBUI.__sendError = function (code, text) {

    SBUI.Errors[code] = true;

    var msg = {
        id: this.__guid(),
        type: 'status',
        eventId: SBUI.Config.eventId,
        data: {
            status: 'Error',
            error: {
                code: code,
                message: text
            }
        }
    };
    this.__postToParent(msg);
};

/**
 * Clear errors
 *
 * @private
 */
SBUI.__clearError = function (code, text) {
    if (SBUI.Errors[code]) {
        delete SBUI.Errors[code];
        var msg = {
            id: this.__guid(),
            type: 'status',
            eventId: SBUI.Config.eventId,
            data: {
                status: 'Ok',
                error: {
                    code: code,
                    message: text
                }
            }
        };
        this.__postToParent(msg);
    }
};

/**
 * Show the main UI
 */
SBUI.__showUI = function () {

    if (!SBUI.showingUI) {
        $('#progress_bar').hide();
        $('#scoreboard_holder').show();

        setTimeout(function () {
            $('#main-wrapper').trigger("heightChange");
        }, 10);

        this.__initUI();
        SBUI.showingUI = true;
    }
};

/**
 * Hide the main UI
 */
SBUI.__hideUI = function () {

    $('#scoreboard_holder').hide();
    $('#progress_bar').show();

    setTimeout(function(){
        $('#main-wrapper').trigger( "heightChange");
    }, 10);
};

/**
 * Initialise the UI components
 */
SBUI.__initUI = function () {

    // Check window width
    SBUI.windowWidth = $(window).width();

    // Check window width on resize
    $(window).on('resize', function(){
        SBUI.windowWidth = $(window).width();
    });

    // Commentary button
    $('.view-switch').on("click", function () {

        if(SBUI.windowWidth > SBUI.breakPoint){
            if(!($('.view-switch li').hasClass('active'))){
                $('.view-switch li').addClass('active');
                $('.commentary').css('display','block');
            } else {
                $('.view-switch li').removeClass('active');
                $('.commentary').css('display','none');
            }
        } else {
            if(!($('.view-switch li').hasClass('active'))){
                $('.view-switch li').addClass('active');
                $('.left').hide();
                $('.right').show();
                $('.commentary').css('display','block');
                $('.stat-switch').removeClass('active');
                $('.stat-switch').css('opacity', '0.56');
            } else {
                $('.view-switch li').removeClass('active');
                $('.animations').show();
                $('.right').css('display','block');
                $('.left').hide();
                $('.commentary').css('display','none');
            }
        }
});

    // Stats button
    $('.stat-switch').on("click", function () {
        if(SBUI.windowWidth < SBUI.breakPoint){
            if(!($('.stat-switch').hasClass('active'))){
                $('.left').show();
                $('.right').hide();
                $('.commentary').css('display','none');
                $('.stat-switch').addClass('active');
                $('.overview').removeClass('active');
                $('.view-switch li').removeClass('active');
            } else {
                $('.left').hide();
                $('.right').show();
                $('.stat-switch').removeClass('active');
                $('.view-switch li').removeClass('active');
            }
        }
    });

    //tabs switch
    $('.tab-container .tab-list').on( "click","li:not(.disabled)", function() {
        $('#'+$( this ).parent().parent().attr('id')+'.tab-container > .tab-list li').each(function(){
            $( this ).removeClass("active");
        });
        $( this ).addClass("active");

        var clickedID = $( this ).parent().parent().attr('id');

        $('#'+clickedID+' > .tab-content > .tab-pane').each(function(xxx){
            $('#'+clickedID+' > .tab-content > .tab-pane').removeClass("active");
        });
        var id=$( this ).children( 'a' ).data( "toggle" );
        $('.tab-container .tab-content #'+id ).addClass("active");

        $('#main-wrapper').trigger( "heightChange");
    });

    //nice scrollers
    jQuery('.commentary ul').niceScroll({
        autohidemode: true,     // Do not hide scrollbar when mouse out
        cursorborderradius: '0px', // Scroll cursor radius
        railpadding: { top: 0, right: 3, left: 0, bottom: 1 },
        background: 'transparent',     // The scrollbar rail color
        cursorwidth: '2px',       // Scroll cursor width
        cursorcolor: '#ffffff',     // Scroll cursor color
        cursorborder: 'none'     // Scroll cursor color
    });


    // Listen for orientation changes
    window.addEventListener("orientationchange", function() {
        setTimeout(function(){
            $('#main-wrapper').trigger( "heightChange");
        }, 300);
    }, false);

    // Call functions based on window resize events
    $(window).on('resize', function(){
        SBUI.__sendResize();
    });
};


SBUI.__InitStreaming = function (id) {
	if (typeof SBUI.Config.params.streamingAvailable !== 'undefined' && SBUI.Config.params.streamingAvailable === 'true') {
		/**
		 * Loads the StreamPlayer lib
		 *
		 * @param  {Function} callback     callback once the script has been loaded.
		 *
		 * @retuns {void}
		 */
		var getStreamingFactoryScript = function (callback) {
			var script = document.createElement('script');
			script.src = SBUI.Config.streamingLibrary;

			document.body.appendChild(script);
			function checkStreamPlayerLoaded() {
				if (window.WH && window.WH.streamingFactory) {
					callback();
				} else {
					setTimeout(checkStreamPlayerLoaded, 100);
				}
			}
			checkStreamPlayerLoaded();
		};

		getStreamingFactoryScript(function () {

			var errorHandler = function (errorCode, errorText) {
				if (SBUI.Config.debug) {
					console.log('errorHandler');
					console.log(errorCode);
					console.log(errorText);
				}
				fail(errorCode);
			};

			var success = function () {
				if (SBUI.Config.debug) {
					console.log('success');
				}

				$('#stream-error-msg').hide();
				$('.streaming-content').addClass('streaming-active');

				$('.bottom-strip .nav-bar li').click(function() {
					if ($('#stream_container').is(":visible")) {
						if (SBUI.Config.debug) {
							console.log('play');
						}
						player.play();
					} else {
						if (SBUI.Config.debug) {
							console.log('pause');
						}
						player.pause();
					}
				});
			};

			var fail = function (errorCode) {
				if (SBUI.Config.debug) {
					console.log('fail');
					console.log(errorCode);
				}

				$('#stream-error-msg').show();
				$('.streaming-content').removeClass('streaming-active');

				var msg = '';

				switch (errorCode) {
					case 'GEO_BLOCKED':
						msg = 'Sorry, live streaming is currently unavailable in your country due to broadcaster territory restrictions.';
						break;
					case 'INSUFFICIENT_FUNDS':
						msg = 'Sorry, you must have placed a bet in order to view this stream.';
						break;
					case 'NOT_LOGGED_IN':
					case 'NO_TOKEN':
					case 'NOT_LOGGED':
						msg = 'Please log-in to watch live streaming. Note: Live streaming is free to active logged in users subject to broadcaster territory restrictions.';
						break;
					case 'STREAM_NOT_AVAILABLE':
						msg = 'Stream unavailable.';
						break;
					case 'EVENT_NOT_STARTED':
						msg = 'Event not started';
						break;
					case 'EVENT_NOT_STARTED_MOBILE':
						msg = 'Event not started<br>Please tap here to refresh';
						break;
					case 'EVENT_OVER':
						msg = 'Event is over';
						break;
					case 'EVENT_NOT_FOUND':
						msg = 'Event not found';
						break;
					case 'ERROR':
					case 'NO_PARENT':
					case 'TIMEOUT':
						msg = 'Sorry, unable to access video server.';
						break;
					default:
						msg = 'Stream unavailable.';
						break;
				}
				$('#stream-error-msg p').html(msg);
			};

			player = window.WH.streamingFactory({
				eventId: id,
				container: '#streaming-content',
				baseUrl: SBUI.Config.streamingBaseUrl,
				self: 'RED7',
				errorHandler: errorHandler
			});

			player.checkAuth('*', success, fail);
		});

	}
};


/**
 * Add a remove function to the Array Prototype
 *
 * @param from
 * @param to
 * @returns {Number}
 */
Array.prototype.remove = function(from, to) {
    var rest = this.slice((to || from) + 1 || this.length);
    this.length = from < 0 ? this.length + from : from;
    return this.push.apply(this, rest);
};

/**
 * Load the framework
 */

$( window ).on("load", function() {

    // Sort out event ID
    if (typeof SBUI.Config.params.eventId !== 'undefined') {
        SBUI.Config.eventId = SBUI.Config.params.eventId;
        SBUI.Config.eventKey = 'id';
    } else if (typeof SBUI.Config.params.internalIds !== 'undefined' && SBUI.__countProperties(SBUI.Config.params.internalIds) === 1) {
        for (var i in SBUI.Config.params.internalIds) {
            SBUI.Config.eventId = SBUI.Config.params.internalIds[i];
            SBUI.Config.eventKey = 'internalIds.' + i ;
        }
    } else {
        SBUI.__sendError(2001, 'Invalid Event Id');
        $('#progress_bar span').text('Invalid Event Id');
        $('#progress_bar i').remove();
        return false;
    }

    // Send initial size info
    SBUI.__sendResize();

    // Check for availability of streaming
    if (typeof SBUI.Config.params.streamingAvailable === 'undefined' || (typeof SBUI.Config.params.streamingAvailable !== 'undefined' && SBUI.Config.params.streamingAvailable == 'false')) {
        $('#stream_container').remove();
        $('#stream').remove();
    }

    // Listen for height change events
    $('#main-wrapper').on( "heightChange", function() {

        //SBUI.__height = $('#main-wrapper').height();
        SBUI.__sendResize();
    });

    // Start the socket connection
    if (SBUI.Config.connection == 'socket') {
        var connection = new SBUI.SocketService();
    }



// Checks if desktop or mobile
//     function checkSize() {


    // Desktop



    // Commentary
    // $('.view-switch').on("click", function () {
    //
    //






        // if(SBUI.windowWidth === 'desktop'){
        //     console.log('commentary desktop')
        //
        //     if(!($('.view-switch li').hasClass('active'))){
        //         $('.view-switch li').addClass('active');
        //         $('.commentary').css('display','block');
        //         } else{
        //             $('.view-switch li').removeClass('active');
        //             $('.commentary').css('display','none');
        //         }
        //         else{
        //
        //         console.log('commentary mobile')
        //
        //
        //         $('.view-switch li').addClass('active');
        //         $('.stat-switch').removeClass('active');
        //         $('.stat-switch').css('opacity', '0.56');
        //         $('.overview').removeClass('active');
        //         $('.overview').css('opacity', '0.56');
        //         $('.left').hide();
        //         $('.right').show();
        //         $('.commentary').css('display', 'block');
        //     } else{
        //         $('.view-switch li').removeClass('active');
        //         $('.overview').addClass('active');
        //         $('.animations').show();
        //         $('.right').css('display','block');
        //         $('.left').hide();
        //         $('.commentary').css('display','none');
        //     }
        //
        // })







});

