/**
 * Socket Service.
 */
{

    SBUI.SocketService = (function() {

        var _this = this;

        this.connection = io(SBUI.Config.socketUrl);

        this.currentTime = [0, 0];
        this.periodTimeLeft = [0, 0];

        setInterval(function () {
            _this.updateTimer();
        }, 1000);

        this.connection.on('connect', function () {

            var authParams = {token: SBUIsocketToken, resource: "eventUpdate"};
            _this.connection.emit('authentication', authParams);

            _this.connection.on('authenticated', function () {
                if (SBUI.Config.debug) {
                    //console.log('Authenticated!');
                }

                _this.top = new SBUI.Top();
                _this.stats = new SBUI.Stats();
                _this.matchActions = new SBUI.MatchActions();
                _this.lineups = new SBUI.Lineups();
                _this.match = new SBUI.Animations();
                _this.processActions = new SBUI.ProcessActions();

                SBUI.homeTeam = 'Competitor A';
                SBUI.awayTeam = 'Competitor B';
                SBUI.homeTeamObject = 0;
                SBUI.awayTeamObject = 1;
                SBUI.homeTeamOrder = 0;
                SBUI.awayTeamOrder = 1;
                SBUI.homeTeamID = 0;
                SBUI.awayTeamID = 0;
                //SBUI.homeTeamPowerplay = false;
                //SBUI.awayTeamPowerplay = false;
                SBUI.currentPeriod = '0';
                SBUI.currentPeriodNumber = 0;

                SBUI.statsData = [];

                //SBUI.fastDisplay = true;
                SBUI.finished = false;
                SBUI.showingTargetStats = true;
                SBUI.possessionOverlay = jQuery('.home-possession-overlay');
                SBUI.animateOverlay = false;


                _this.connection.on('connect_error', function () {
                    if (SBUI.Config.debug) {
                        console.log('Connection Failed');
                    }
                    SBUI.__sendError(1001, 'Cant connect to socket');
                });
                _this.connection.on("failure", function (data) {
                    SBUI.__sendError(data.msg, data.msg);
                    jQuery('#progress_bar span').text(data.msg);
                    jQuery('#progress_bar i').remove();
                    return false;
                });
            });
            _this.connection.on('unauthorized', function (data) {
                SBUI.__sendError(1004, data.message);
                jQuery('#progress_bar span').text(data.message);
                jQuery('#progress_bar i').remove();
                return false;
            });
        });

        _this.connection.on('init', function (data) {

            if (SBUI.Config.debug) {
                console.log(data);
            }
            data = data.data;

            //<editor-fold desc="initialisation">
            if (_this.matchStatusCheck(data)) {
            	if (!SBUI.showingUI) {
					SBUI.__showUI();
					//streaming
					if (typeof data.internalIds.williamhill !== 'undefined') {
						SBUI.__InitStreaming(data.internalIds.williamhill);
					}
				}
                //set current period
                if (typeof data.matchStatus !== 'undefined' && typeof data.matchStatus.period !== 'undefined' ) {
                    SBUI.currentPeriod = data.matchStatus.period;
                    SBUI.currentPeriodNumber = _this.getPeriodNumber(SBUI.currentPeriod);
                }
                //set team names
                if (typeof data.teams !== 'undefined' ) {
                    SBUI.Top.prototype.updateTeamNames(data.teams);
                }
                //set current time
                if (typeof data.matchStatus !== 'undefined' && typeof data.matchStatus.time !== 'undefined' && typeof data.matchStatus.periodTimeRemaining !== 'undefined' ) {
                    _this.updateCurrentTime(data.matchStatus);
                }
                //update yellow box
                if (typeof data.matchStatus !== 'undefined' && typeof data.matchStatus.score !== 'undefined' ) {
                    SBUI.Top.prototype.updateMatchScore(data.matchStatus.score);
                }

                //TODO ADD/REMOVE DEPENDING ON SHOTS ON/OFF TARGET
                _this.toggleTargetRows(false);

                //populate actions (1)
                if (typeof data.actions !== 'undefined' ){
                    SBUI.MatchActions.prototype.populate(data.actions);
                }
                //display actions (2)
                /*if (typeof data.actions !== 'undefined' ){
                    if (SBUI.Config.debug)
                    {
                        console.log('displaying actions...');
                    }
                    SBUI.Animations.prototype.displayAction(data.actions);
                }*/
                //populate lineups (3)
                if (typeof data.actions !== 'undefined' ){
                    SBUI.Lineups.prototype.populateLineups(data.actions, true);
                }

                //read in goals on pp stats
                /*if (typeof data.teams !== 'undefined' ) {
                    SBUI.Lineups.prototype.readGoalsOnPP(data.teams);
                }*/
            } else {
                _this.connection.disconnect();
            }
            //</editor-fold>
        });


        _this.connection.on('update', function (data) {
            if (SBUI.Config.debug) {
                // console.log(data);


            }
            data = data.data;
            //On update check match status
            if (!_this.matchStatusCheck(data)) {
                _this.connection.disconnect();
                SBUI.__hideUI();
                return false;
            }
            //On update check if full refresh object or partial update
            if (typeof data.options !== 'undefined' && typeof data.options.refresh !== 'undefined' && data.options.refresh === "true") {
                if (SBUI.Config.debug) {
                    console.log('refresh');
                }
                jQuery('#comment_container ul').empty();
                SBUI.statsData = [];

            }
            //<editor-fold desc="update">

            //set current period
            if (typeof data.matchStatus !== 'undefined' && typeof data.matchStatus.period !== 'undefined' ) {
                if (SBUI.currentPeriod !== data.matchStatus.period && data.matchStatus.period === "0") {
                    console.log('restarting match...');
                    //reset stats
                    _this.resetStats();
                }
                else {
                    SBUI.currentPeriod = data.matchStatus.period;
                    SBUI.currentPeriodNumber = _this.getPeriodNumber(SBUI.currentPeriod);
                }
            }

            //set current time
            if (typeof data.matchStatus !== 'undefined' && typeof data.matchStatus.time !== 'undefined' && typeof data.matchStatus.periodTimeRemaining !== 'undefined' ) {
                _this.updateCurrentTime(data.matchStatus);
            }

            //team names
            if (typeof data.teams !== 'undefined' ) {
                SBUI.Top.prototype.updateTeamNames(data.teams);
            }

            //update yellow box
            if (typeof data.matchStatus !== 'undefined' && typeof data.matchStatus.score !== 'undefined' ) {
                SBUI.Top.prototype.updateMatchScore(data.matchStatus.score);
            }

            //populate actions (1)
            if (typeof data.actions !== 'undefined' ){
                SBUI.MatchActions.prototype.populate(data.actions);
            }
            /*//display actions (2)
            if (typeof data.actions !== 'undefined' ){
                SBUI.Animations.prototype.displayAction(data.actions);
            }*/
            //populate lineups (3)
            if (typeof data.actions !== 'undefined' ){
                SBUI.Lineups.prototype.populateLineups(data.actions, false);
            }

            //read in goals on pp stats
            /*if (typeof data.teams !== 'undefined' ) {
                SBUI.Lineups.prototype.readGoalsOnPP(data.teams);
            }*/

            //<editor-fold desc="beans">

            //</editor-fold>

            //</editor-fold>
        });

    });

    SBUI.SocketService.prototype.matchStatusCheck = function (data) {
        if (typeof data.matchStatus !== 'undefined' && typeof data.matchStatus.status !== 'undefined') {
            switch (data.matchStatus.status) {
                case 'pre':
                    SBUI.MatchActions.prototype.changePossession();
                    SBUI.finished = false;
                    this.lineups.paused = true;
                    return true;
                case 'live':
                    this.lineups.paused = false;
                    SBUI.finished = false;
                    return true;
                case 'break':
                    SBUI.MatchActions.prototype.changePossession();
                    SBUI.finished = false;
                    this.lineups.paused = true;
                    return true;
                case 'cancelled':
                    SBUI.MatchActions.prototype.changePossession();
                    SBUI.finished = false;
                    this.lineups.paused = true;
                    return true;
                case 'finished':
                    var result = '';
                    if (typeof data.teams !== 'undefined' && typeof data.teams[0] !== 'undefined' && typeof data.teams[0].gameData !== 'undefined' && typeof data.teams[0].gameData.isWinner !== 'undefined' && data.teams[0].gameData.isWinner === 'true') {
                        if (typeof data.teams[0].team !== 'undefined' && typeof data.teams[0].team.name !== 'undefined') {
                            result = 'Winner: ' + data.teams[0].team.name;
                        }
                        else {
                            result = 'Winner: ' + SBUI.homeTeam;
                        }
                    }
                    else if (typeof data.teams !== 'undefined' && typeof data.teams[1] !== 'undefined' && typeof data.teams[1].gameData !== 'undefined' && typeof data.teams[1].gameData.isWinner !== 'undefined' && data.teams[1].gameData.isWinner === 'true') {
                        if (typeof data.teams[1].team !== 'undefined' && typeof data.teams[1].team.name !== 'undefined') {
                            result = 'Winner: ' + data.teams[1].team.name;
                        }
                        else {
                            result = 'Winner: ' + SBUI.awayTeam;
                        }
                    }
                    else {
                        result = 'Draw';
                    }
                    SBUI.MatchActions.prototype.changePossession();
                    SBUI.finished = true;
                    this.lineups.paused = true;
                    return true;
                case 'suspended':
                    SBUI.MatchActions.prototype.changePossession();
                    SBUI.finished = false;
                    this.lineups.paused = true;
                    return true;
                case 'postponed':
                    SBUI.MatchActions.prototype.changePossession();
                    SBUI.finished = false;
                    this.lineups.paused = true;
                    return true;
            }
        }else{
        	return true;
        }
    };

    //used for turning on/off 'Shots On/Off Target' stats
    SBUI.SocketService.prototype.toggleTargetRows = function(show) {
        if (SBUI.showingTargetStats !== show) {
            SBUI.showingTargetStats = show;
            var goalRows = document.getElementsByClassName('goal-row');
            var penaltyRows = document.getElementsByClassName('penalty-row');
            var targetRows = document.getElementsByClassName('target-row');
            var rows = targetRows.length;
            for (var i = 0; i < rows; i++) {
                if (show) {
                    jQuery(targetRows[i]).show();
                    jQuery(penaltyRows[i]).css({'height': '33%'});
                    jQuery(goalRows[i]).css({'height': '33%'});
                }
                else {
                    jQuery(targetRows[i]).hide();
                    jQuery(penaltyRows[i]).css({'height' : '50%'});
                    jQuery(goalRows[i]).css({'height' : '50%'});
                }
            }
        }
    }

    //<editor-fold desc="Period String Replacement Functions">

    //get time values
    SBUI.SocketService.prototype.updateCurrentTime = function (matchStatus) {
        animate = typeof animate !== 'undefined' ? animate : true;
        //try {
            var RemainingTime = matchStatus.periodTimeRemaining;
            this.periodTimeLeft[0] = parseInt(RemainingTime.split(':')[0]);
            this.periodTimeLeft[1] = parseInt(RemainingTime.split(':')[1]);

            var PassedTime = matchStatus.time;
            this.currentTime[0] = parseInt(PassedTime.split(':')[0]);
            this.currentTime[1] = parseInt(PassedTime.split(':')[1]);
            SBUI.Top.prototype.updateTimerDisplays(this.getTimeLeft(this.periodTimeLeft));

        /*} catch (err) {
            if (SBUI.Config.debug) {
                console.log("Couldn't update current time");
                console.log(err);
            }
        }*/
    }

    //used for retrieving the period integer from the original string
    SBUI.SocketService.prototype.getPeriodNumber = function (period) {
        switch (period){
            case '0':
            case '1':
            case '2':
            case '3':
                return parseInt(period);
            case 'ST':
                return 5
            default:
                if (period.startsWith("OT")) {
                    return 4;
                }
                else
                {
                    return 0;
                }
        }
    }

    //used for the small scoreboard at the top.
    SBUI.SocketService.prototype.replacePeriod = function() {
        switch (SBUI.currentPeriod){
            case '0':
                return 'Pre-Game'
            case '1':
                return '1st';
            case '2':
                return '2nd';
            case '3':
                return '3rd';
            case 'ST':
                return 'SO';
            default:
                if (SBUI.currentPeriod.startsWith("OT"))
                {
                    return 'OT';
                }
                else
                {
                    return '???';
                }
        }
    }

    //used for the footer box
    SBUI.SocketService.prototype.replacePeriodFull = function() {
        switch (SBUI.currentPeriod){
            case '0':
                return 'Pre-Game';
            case '1':
                return 'Period 1';
            case '2':
                return 'Period 2';
            case '3':
                return 'Period 3';
            case 'ST':
                return 'Shootout';
            default:
                if (SBUI.currentPeriod.startsWith('OT'))
                {
                    return 'Overtime';
                }
                else
                {
                    return '???';
                }
        }
    }

    //divides a timer string into seperate integers
    SBUI.SocketService.prototype.getTimeLeft = function(timer) {
        var mins = timer[0];
        var secs = timer[1];
        if (mins.toString().length === 1)
        {
            mins = '0' + mins;
        }
        if (secs.toString().length === 1)
        {
            secs = '0' + secs;
        }
        return (mins + ':' + secs);
    }

    //</editor-fold>

    SBUI.SocketService.prototype.updateTimer = function () {
        if (!SBUI.Lineups.paused && (this.periodTimeLeft[0] >0 || this.periodTimeLeft[1] >0)) {
            this.periodTimeLeft[1] -= 1;
            if (this.periodTimeLeft[1] < 0) {
                this.periodTimeLeft[1] += 60;
                this.periodTimeLeft[0]--;
            }
            this.currentTime[1] += 1;
            if (this.currentTime[1] >= 60) {
                this.currentTime[0] += 1;
                this.currentTime[1] -= 60;
            }
            SBUI.Top.prototype.updateTimerDisplays(this.getTimeLeft(this.periodTimeLeft));

            //control pulsing of possession overlay
            if (SBUI.animateOverlay) {
				SBUI.possessionOverlay.animate({opacity: 0.6}, 500, function () {
					SBUI.possessionOverlay.animate({opacity: 1}, 500);
				});
			}
        }
    }

    //resets everything when the match starts again
    SBUI.SocketService.prototype.resetStats = function () {
        //reset stats
        SBUI.statsData = [];
        //Powerplay goals must be reset separately
        SBUI.Lineups.homeTotalGoalsOnPP = 0;
        SBUI.Lineups.awayTotalGoalsOnPP = 0;
        //the match is no longer finished
        SBUI.finished = false;
        //reset timers
        SBUI.currentTime = [0, 0];
        SBUI.periodTimeLeft = [20, 0];
        //reset periods
        SBUI.currentPeriod = '0';
        SBUI.currentPeriodNumber = 0;
        //reset top score
        SBUI.Top.prototype.updateMatchScore("0:0");
        //empty commentary
        jQuery('#comment_container ul').empty();

        //grey-out scoreboard
        jQuery('.1stScore').text('-');
        jQuery('.1stScore').css({'color' : '#A6B4CA'});
        jQuery('.1stHeader').css({'color' : '#cccc2c'});
        jQuery('.2ndScore').text('-');
        jQuery('.2ndScore').css({'color' : '#A6B4CA'});
        jQuery('.2ndHeader').css({'color' : '#cccc2c'});
        jQuery('.3rdScore').text('-');
        jQuery('.3rdScore').css({'color' : '#A6B4CA'});
        jQuery('.3rdHeader').css({'color' : '#cccc2c'});
        jQuery('.OTScore').text('-');
        jQuery('.OTScore').css({'color' : '#A6B4CA'});
        jQuery('.OTHeader').css({'color' : '#cccc2c'});
        jQuery('.STScore').text('-');
        jQuery('.STScore').css({'color' : '#A6B4CA'});
        jQuery('.STHeader').css({'color' : '#cccc2c'});

        //remove OT & SO stat tabs if present
        jQuery('.tab-list').children().slice(2).remove();
        jQuery('.scoreHeaders').children().slice(3).remove();
        jQuery('.individualScores').children().slice(3).remove();

        //turn off target rows
        this.toggleTargetRows(false);
        //reset bottom clock
        SBUI.Top.prototype.updateTimerDisplays('00:00');
        SBUI.Animations.prototype.showBlackBoard('Match coming soon','','start','*');
        //remove possession puck
        SBUI.MatchActions.prototype.changePossession();
        //refresh stats
        SBUI.Lineups.prototype.calculateStats();
        SBUI.Stats.prototype.displayStats();
    }
}

