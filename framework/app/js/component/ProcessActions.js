{
    SBUI.ProcessActions = (function () {
        var instance;
        SBUI.ProcessActions = function () {
            return instance;
        };

        SBUI.ProcessActions.prototype = this;

        instance = new SBUI.ProcessActions();
        instance.constructor = SBUI.ProcessActions;
        return instance;

    });

    SBUI.ProcessActions.prototype.populate = function (actions, initialising, display) {

        var periodNumber = 0;
        var keys = [];
        for (var key in actions) {
            if (actions.hasOwnProperty(key) && actions[key].actionId === '1013') {
                periodNumber = parseInt(actions[key].period);
                if (typeof SBUI.statsData[periodNumber] === 'undefined') {
                    SBUI.statsData[periodNumber] = [];
                }
            }
            if (initialising) {
                if (periodNumber > 0) {
                    SBUI.statsData[periodNumber].push(actions[key]);
                }
            }
            else if (parseInt(actions[key].period) > 0) {
                SBUI.statsData[parseInt(actions[key].period)].push(actions[key]);
            }
            keys.push(key);
        }

        keys.sort();
        keys.pop();
        for (var i = 0; i < keys.length; i++) {
            this.add(actions[keys[i]], false, initialising);
        }
        //add last action
        this.add(actions[key], true, initialising);

        if (initialising) {
            SBUI.Stats.prototype.populateStats();
        }
        if (display) {
            SBUI.Stats.prototype.displayStats(false);
        }
    };
}
