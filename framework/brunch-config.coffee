outputdir = '../html/'

exports.config =
# See http://brunch.io/#documentation for docs.
  files:
    javascripts:
      joinTo:
        'js/app.js': [
          /^app\/js\/.*\.js$/
        ]
        'js/lib.js': /^(bower_components|vendor)/
      order:
        before: [
          'app/js/Init.js'
          'app/js/Config.js'
          'app/js/component/*.js'
          'app/js/service/*.js'
        ]
    stylesheets:
      joinTo:
        'css/app.css': [
          /^app\/css/
          /^(bower_components|vendor)/
        ]
    templates:
      joinTo: 'js/app.js'
  paths:
    public: outputdir
  modules:
    wrapper: false
  plugins:
    htmlPages:
      pattern: /\.php$/
    uglify:
      mangle: true
      compress:
        global_defs:
          DEBUG: false
      mt: true
      beautify: false
      properties: false
  sourceMaps:true
  optimize:true
  overrides:
    production:
      plugins:
        afterBrunch: [
          'java -jar closure/compiler.jar
          --compilation_level ADVANCED_OPTIMIZATIONS
          --externs '+outputdir+'/js/lib.js
          --js=' + outputdir + '/js/app.js
          --warning_level QUIET
          --js_output_file=' + outputdir + '/js/app-min.js;
          rm ' + outputdir + '/js/app.js;
          mv ' + outputdir + '/js/app-min.js ' + outputdir + '/js/app.js;'
        ]