<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="HandheldFriendly" content="True">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="author" content="red7mobile">
    <link rel="shortcut icon" href="favicon.ico">
    <title>Scoreboard - Ice Hockey</title>
    <link href="css/app.css" rel="stylesheet">
    <script type="application/javascript">
        <?php require_once ('../../auth.php'); ?>
        var SBUIsocketToken = "<?php echo (isset($_GET['eventId']) ? Authentication::getToken($_GET['eventId']) : ''); ?>";
    </script>
    <script src="js/lib.js"></script>
    <script src="js/app.js"></script>
</head>
<body>
<noscript>
    Your browser doesn't support JavaScript or you have disabled JavaScript. This website require javascript for proper
    functioning. Please visit <a href="http://support.microsoft.com/gp/howtoscript">How to enable JavaScript in a web
    browser</a> website or use different browser.
</noscript>
<div id="progress_bar">
    <div class="loading">Loading...</div>
    <div class="spinner">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
    </div>
    <img src="images/skybet_logo.png"/>
</div>
<div id="main-wrapper">
	<div id="scoreboard_holder">
        <div class="top">
                <div class="score-headers">
                    <div class="labels-total"><span>Total</span></div>
                    <div class="label label-p1"><span>P1</span></div>
                    <div class="label label-p2"><span>P2</span></div>
                    <div class="label label-p3"><span>P3</span></div>
                </div>
            <div class="scores">
                <div class="teams">
                    <div class="teamAname"><span></span></div>
                    <div class="teamBname"><span></span></div>
                </div>
                <div class="points">

                    <div class="home-team-points">
                        <div class="puck-home">
                            <img src="images/Badges/testpuck.png">
                        </div>
                        <div class="home-team-total">0</div>
                        <div class="home-team-p1">0</div>
                        <div class="home-team-p2">0</div>
                        <div class="home-team-p3">0</div>
                    </div>
                    <div class="away-team-points">
                        <div class="puck-away">
                            <img src="images/Badges/testpuck.png">
                        </div>
                        <div class="away-team-total">0</div>
                        <div class="away-team-p1">0</div>
                        <div class="away-team-p2">0</div>
                        <div class="away-team-p3">0</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="top-view-switch">
            <div class="stat-switch">Stats<span class="icon-stats"></span></div>
        </div>

        <div class="main">
            <div class="left">
                <div id="stats" class="stats tab-container active">
                        <ul class="tab-list">
                            <li class="active"><a href="#" data-toggle="stats-total"><span>Total</span></a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="stats-total">
                                <table class="stats-table">
                                    <tr>
                                        <td>
                                            <div class="label">Goals</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar goals-total-bar"></div>
                                                    <div class="leftResult home-total-goals"></div>
                                                    <div class="rightResult away-total-goals"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="label">Goals on PP</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar goals-on-pp-bar"></div>
                                                    <div class="leftResult home-total-goals-on-pp"></div>
                                                    <div class="rightResult away-total-goals-on-pp"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="label">PP Minutes</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar pp-minutes-bar"></div>
                                                    <div class="leftResult home-total-pp-minutes"></div>
                                                    <div class="rightResult away-total-pp-minutes"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="label">Penalties</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar penalties-total-bar"></div>
                                                    <div class="leftResult home-total-penalties"></div>
                                                    <div class="rightResult away-total-penalties"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="label">Shots on Target</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar on-target-total-bar"></div>
                                                    <div class="leftResult home-total-on-target"></div>
                                                    <div class="rightResult away-total-on-target"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="label">Shots off Target</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar off-target-total-bar"></div>
                                                    <div class="leftResult home-total-off-target"></div>
                                                    <div class="rightResult away-total-off-target"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="tab-pane" id="stats-1">
                                <table class="stats-table">
                                    <tr>
                                        <td>
                                            <div class="label">Goals</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar goals-1-bar"></div>
                                                    <div class="leftResult goals-1-home"></div>
                                                    <div class="rightResult goals-1-away"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="label">Goals on PP</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar goals-1-pp-bar"></div>
                                                    <div class="leftResult goals-1-pp-home"></div>
                                                    <div class="rightResult goals-1-pp-away"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="label">PP Minutes</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar pp-mins-1-bar"></div>
                                                    <div class="leftResult pp-mins-1-home"></div>
                                                    <div class="rightResult pp-mins-1-away"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="label">Penalties</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar penalties-1-bar"></div>
                                                    <div class="leftResult penalties-1-home"></div>
                                                    <div class="rightResult penalties-1-away"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="label">Shots on Target</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar on-target-1-bar"></div>
                                                    <div class="leftResult on-target-1-home"></div>
                                                    <div class="rightResult on-target-1-away"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="label">Shots off Target</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar off-target-1-bar"></div>
                                                    <div class="leftResult off-target-1-home"></div>
                                                    <div class="rightResult off-target-1-away"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="tab-pane" id="stats-2">
                                <table class="stats-table">
                                    <tr>
                                        <td>
                                            <div class="label">Goals</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar goals-2-bar"></div>
                                                    <div class="leftResult goals-2-home"></div>
                                                    <div class="rightResult goals-2-away"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="label">Goals on PP</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar goals-2-pp-bar"></div>
                                                    <div class="leftResult goals-2-pp-home"></div>
                                                    <div class="rightResult goals-2-pp-away"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="label">PP Minutes</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar pp-mins-2-bar"></div>
                                                    <div class="leftResult pp-mins-2-home"></div>
                                                    <div class="rightResult pp-mins-2-away"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="label">Penalties</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar penalties-2-bar"></div>
                                                    <div class="leftResult penalties-2-home"></div>
                                                    <div class="rightResult penalties-2-away"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="label">Shots on Target</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar on-target-2-bar"></div>
                                                    <div class="leftResult on-target-2-home"></div>
                                                    <div class="rightResult on-target-2-away"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="label">Shots off Target</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar off-target-2-bar"></div>
                                                    <div class="leftResult off-target-2-home"></div>
                                                    <div class="rightResult off-target-2-away"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="tab-pane" id="stats-3">
                                <table class="stats-table">
                                    <tr>
                                        <td>
                                            <div class="label">Goals</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar goals-3-bar"></div>
                                                    <div class="leftResult goals-3-home"></div>
                                                    <div class="rightResult goals-3-away"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="label">Goals on PP</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar goals-3-pp-bar"></div>
                                                    <div class="leftResult goals-3-pp-home"></div>
                                                    <div class="rightResult goals-3-pp-away"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="label">PP Minutes</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar pp-mins-3-bar"></div>
                                                    <div class="leftResult pp-mins-3-home"></div>
                                                    <div class="rightResult pp-mins-3-away"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="label">Penalties</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar penalties-3-bar"></div>
                                                    <div class="leftResult penalties-3-home"></div>
                                                    <div class="rightResult penalties-3-away"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="label">Shots on Target</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar on-target-3-bar"></div>
                                                    <div class="leftResult on-target-3-home"></div>
                                                    <div class="rightResult on-target-3-away"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="label">Shots off Target</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar off-target-3-bar"></div>
                                                    <div class="leftResult off-target-3-home"></div>
                                                    <div class="rightResult off-target-3-away"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="tab-pane" id="stats-4">
                                <table class="stats-table">
                                    <tr>
                                        <td>
                                            <div class="label">Goals</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar goals-4-bar"></div>
                                                    <div class="leftResult goals-4-home"></div>
                                                    <div class="rightResult goals-4-away"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="label">Goals on PP</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar goals-4-pp-bar"></div>
                                                    <div class="leftResult goals-4-pp-home"></div>
                                                    <div class="rightResult goals-4-pp-away"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="label">PP Minutes</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar pp-mins-4-bar"></div>
                                                    <div class="leftResult pp-mins-4-home"></div>
                                                    <div class="rightResult pp-mins-4-away"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="label">Penalties</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar penalties-4-bar"></div>
                                                    <div class="leftResult penalties-4-home"></div>
                                                    <div class="rightResult penalties-4-away"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="label">Shots on Target</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar on-target-4-bar"></div>
                                                    <div class="leftResult on-target-4-home"></div>
                                                    <div class="rightResult on-target-4-away"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="label">Shots off Target</div>
                                            <div class="barFrame">
                                                <div class="bar">
                                                    <div class="leftBar off-target-4-bar"></div>
                                                    <div class="leftResult off-target-4-home"></div>
                                                    <div class="rightResult off-target-4-away"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                </div>
                <div class="bottom">
                    <div class="latest-action"></div>
                    <ul class="view-switch">
                        <li>Commentary<span class="icon-comment-icon"></span></li>
                    </ul>
                </div>
            </div>
            <div class="right">
                <div class="scoreboard">
                <div class="animations">
<!--                    <div class="message">Live Hockey Visualization</br><b>(Provided by RED7)</b></div>-->
                    <div class="board-background">
                        <div class="main-pitch">
                            <div class="possession">
                            </div>
                            <div class="pitch-markings"></div>

                                <div class="action-message">
                                    <div class="badge">
                                        <span id="badgePic"></span>
                                    </div>
                                </div>
                            </div>
                        <div class="action-text">
                        </div>
                    </div>
                </div>
                <div id="comment_container" class="commentary">
                    <ul>
                    </ul>
                </div>
            </div>
                <div class="bottom-right">
                    <div class="latest-action"></div>
                    <ul class="view-switch">
                        <li>Commentary<span class="icon-comment-icon"></span></li>
                    </ul>
                </div>
            </div>
    </div>
    </div>
</div>
</body>

