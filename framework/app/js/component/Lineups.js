{
    SBUI.Lineups = (function() {
        var instance;
        SBUI.Lineups = function() {
            return instance;
        };
        SBUI.Lineups.prototype = this;
        SBUI.Lineups.paused = true;
        instance = new SBUI.Lineups();
        instance.constructor = SBUI.Lineups;
        return instance;
    });

    /**
     * Populate statsData with type 1013 actions and separate by periods to arrays for further processing
     * @param {Object} actions
     * @return {Object} SBUI.statsData
     */
    SBUI.Lineups.prototype.populateLineups = function (actions, initialising) {
        //try {
            var periodNumber = 0;
            for (var key in actions) {
                //1013 : period change
                if (actions.hasOwnProperty(key) && actions[key].actionId === '1013') {
                    //correction for last actions --
                    if(actions[key].actionId === "1013"){
                        periodNumber = SBUI.SocketService.prototype.getPeriodNumber(actions[key].period);
                    }
                    if(typeof SBUI.statsData[periodNumber] === 'undefined'){
                        SBUI.statsData[periodNumber] = [];
                    }
                }
                if (initialising) {
                    if (periodNumber > 0) {
                        SBUI.statsData[periodNumber].push(actions[key]);
                    }
                }
                else if (SBUI.SocketService.prototype.getPeriodNumber(actions[key].period) > 0) {
                    SBUI.statsData[SBUI.SocketService.prototype.getPeriodNumber(actions[key].period)].push(actions[key]);
                }
            }
            //console.log('calculating stats');
            SBUI.Lineups.prototype.calculateStats();
            //console.log('displaying stats');
            SBUI.Stats.prototype.displayStats();
            //console.log('all done!');
            SBUI.__clearError(3040, 'Lineups populated');
        /*} catch (err) {
            var msg = 'Could not populate lineups';
            if (SBUI.Config.debug) {
                console.log(msg);
                console.log(err);
            }
            SBUI.__sendError(3040, msg);
        }*/
    };

    /**
     * Create SBUI.Lineups object to hold stats data
     * @return {Object} SBUI.Lineups
     */
    SBUI.Lineups.prototype.calculateStats = function () {
        //try {
        SBUI.Lineups.homeTotalGoals = 0;
        SBUI.Lineups.awayTotalGoals = 0;
        SBUI.Lineups.homeTotalGoalsOnPP = 0;
        SBUI.Lineups.awayTotalGoalsOnPP = 0;
        SBUI.Lineups.homeTotalPPMinutes = 0;
        SBUI.Lineups.awayTotalPPMinutes = 0;
        SBUI.Lineups.homeTotalPenalties = 0;
        SBUI.Lineups.awayTotalPenalties = 0;
        SBUI.Lineups.homeTotalShotsOnTarget = 0;
        SBUI.Lineups.awayTotalShotsOnTarget = 0;
        SBUI.Lineups.homeTotalShotsOffTarget = 0;
        SBUI.Lineups.awayTotalShotsOffTarget = 0;
        SBUI.Lineups.Stats = [];
        for(i = 0; i < SBUI.currentPeriodNumber; i++){
            SBUI.Lineups.Stats[i+1] = {};
            SBUI.Lineups.Stats[i+1].homeGoals = 0;
            SBUI.Lineups.Stats[i+1].awayGoals = 0;
            SBUI.Lineups.Stats[i+1].homeGoalsOnPP = 0;
            SBUI.Lineups.Stats[i+1].awayGoalsOnPP = 0;
            SBUI.Lineups.Stats[i+1].homePPMinutes = 0;
            SBUI.Lineups.Stats[i+1].awayPPMinutes = 0;
            SBUI.Lineups.Stats[i+1].homePenalties = 0;
            SBUI.Lineups.Stats[i+1].awayPenalties = 0;
            SBUI.Lineups.Stats[i+1].homeShotsOnTarget = 0;
            SBUI.Lineups.Stats[i+1].awayShotsOnTarget = 0;
            SBUI.Lineups.Stats[i+1].homeShotsOffTarget = 0;
            SBUI.Lineups.Stats[i+1].awayShotsOffTarget = 0;
        }
        for (var key in SBUI.statsData) {
            if (SBUI.statsData.hasOwnProperty(key)) {
                SBUI.Lineups.prototype.calculatePeriods(key, SBUI.statsData[key]);
            }
        }
        SBUI.__clearError(3041, 'Lineups calculated');
        /*} catch (err) {
            var msg = 'Could not calculate lineups';
            if (SBUI.Config.debug) {
                console.log(msg);
                console.log(err);
            }
            SBUI.__sendError(3041, msg);
        }*/
    };

    /**
     * Calculate stats by legs
     * @param {String} periodKey
     * @param {Object} periodData
     */
    SBUI.Lineups.prototype.calculatePeriods = function (periodKey,periodData) {
        //try {
        //<editor-fold desc="Stat Calculations">
        for (var actionKey in periodData) {
            if (periodData.hasOwnProperty(actionKey)) {
                switch (periodData[actionKey].actionId) {
                    case '30':      //goal
                        SBUI.MatchActions.prototype.goalScored(periodData[actionKey]);
                        break;
                    case '43':      //suspension
                        SBUI.MatchActions.prototype.suspensionGiven(periodData[actionKey]);
                        break;
                    case '60':      //substitution
                        break;
                    case '110':     //possession
                        break;
                    case '156':     //possible goal
                        break;
                    case '157':     //missed goal
                        break;
                    case '161':     //penalty awarded
                        break;
                    case '172':     //missed goal
                        break;
                    case '666':     //penalty missed
                        break;
                    case '1002':    //penalty shot taken
                        SBUI.MatchActions.prototype.penaltyTaken(periodData[actionKey]);
                        break;
                    case '1013':    //period changed
                        //SBUI.MatchActions.prototype.newPeriodStarted(periodData[actionKey]);
                        break;
                    case '1015':    //players on rink
                        break;
                    case '1018':    //possible goal
                        break;
                    case '1019':    //miss
                        break;
                    case '1024':    //warmup
                        break;
                    case '1035':    //timeout
                        break;
                    case '1036':    //time stopped/resumed
                        //SBUI.MatchActions.prototype.pauseUnpause(periodData[actionKey]);
                        break;
                    case '1039':    //time
                        break;
                    case '1042':    //possible penalty shot awarded
                        break;
                    case '1047':    //timeout over
                        break;
                    case '1049':    //suspension over
                        break;
                    case '1050':    //possible empty net
                        break;
                    case '1051':    //empty net
                        break;
                    case '1052':    //empty net over
                        break;
                    case '1104':    //penalty shoot out starting
                        break;
                    case '1112':    //time
                        break;
                    case '1113':    //time
                        break;
                    case '1437':    //timeout
                        break;
                    case '1438':    //timeout
                        break;
                    case '1521':    //timeout
                        break;
                    case '1768':    //possible goal
                        break;
                    case '1769':    //players on rink
                        break;
                    case '1770':    //timeout
                        break;
                    case '1771':    //possible goal
                        break;
                    case '1772':    //possible goal
                        break;
                    case '1885':    //puck position
                        break;
                    default:
                        break;
                }
            }
        }
        //</editor-fold>

        SBUI.__clearError(3042, 'Legs/sets/total stats calculated');
        /*} catch (err) {
            var msg = 'Could not calculate legs/sets/total stats';
            if (SBUI.Config.debug) {
                console.log(msg);
                console.log(err);
            }
            SBUI.__sendError(3042, msg);
        }*/
    };

    SBUI.Lineups.prototype.readGoalsOnPP = function (teams) {
        if (typeof teams[0] !== 'undefined' && typeof teams[0].gameData !== 'undefined' && typeof teams[0].gameData.scoreOnPowerPlay !== 'undefined') {
            if (typeof teams[0].gameData.scoreOnPowerPlay[0] !== 'undefined') {
                SBUI.Lineups.homeTotalGoalsOnPP = teams[0].gameData.scoreOnPowerPlay[0];
            }
            for (i = 0; i < SBUI.currentPeriodNumber; i++) {
                if (typeof teams[0].gameData.scoreOnPowerPlay[i + 1] !== 'undefined') {
                    SBUI.Lineups.Stats[i + 1].homeGoalsOnPP = teams[0].gameData.scoreOnPowerPlay[i + 1];
                }
            }
        }
        if (typeof teams[1] !== 'undefined' && typeof teams[1].gameData !== 'undefined' && typeof teams[1].gameData.scoreOnPowerPlay !== 'undefined') {
            if (typeof teams[1].gameData.scoreOnPowerPlay[1] !== 'undefined') {
                SBUI.Lineups.awayTotalGoalsOnPP = 6;//teams[1].gameData.scoreOnPowerPlay[1];
            }
            for (i = 0; i < SBUI.currentPeriodNumber; i++) {
                if (typeof teams[0].gameData.scoreOnPowerPlay[i + 1] !== 'undefined') {
                    SBUI.Lineups.Stats[i + 1].awayGoalsOnPP = 2;//teams[1].gameData.scoreOnPowerPlay[i + 1];
                }
            }
        }
        //SBUI.Lineups.awayTotalGoalsOnPP = 2;
        SBUI.Stats.prototype.displayStats();
    }

    /**
     * Show/hide lineups data           currently unused
     */
    SBUI.Lineups.prototype.showStats = function (_this){
        if(jQuery(_this).parent().parent().hasClass( "opened" )){
            jQuery(_this).parent().parent().removeClass("opened");
            SBUI.selectedLineup = 0;
        }else{
            jQuery(_this).parent().parent().parent().children().removeClass("opened");
            jQuery(_this).parent().parent().addClass("opened");
            var x = 0;
            jQuery("#events_container ul li").each(function(idx, li) {
                x++;
                if(jQuery(li).hasClass( "opened" )){
                    SBUI.selectedLineupsIdx  = idx;
                }
            });
            SBUI.selectedLineup = x-SBUI.selectedLineupsIdx;
        }
        setTimeout(function(){
            jQuery('#main-wrapper').trigger( "heightChange");
        }, 10);
    };
}